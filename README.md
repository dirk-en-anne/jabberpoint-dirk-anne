# Jabberpoint Dirk Anne

# Sample presentation
Sample presentation can be opened from [/sample/presentation.xml](/sample/presentation.xml)

# Open eclipse project
To open the project in eclipse:
 - go to File->Open projects from file system...
 - Select the first checkbox (see image below) that says 
 - 'jabberpoint-dirk-anne'
 Uncheck the other projects/folders

![Open eclipse project](Documents/eclipse_project.png)