package infrastructure.visitortests;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.jupiter.api.*;

import domain.factories.PresentationElementFactory;
import domain.repositories.PresentationElementCache;
import infrastructure.CreatePresentationElementsVisitor;
import infrastructure.parsermodels.*;

public class CreatePresentationElementsVisitorCallsFactoryTests {

	@Test
    public void visit_slideshow_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationElementFactory.class);
		var slideshowMock = mock(SlideshowParserDTO.class);
		when(slideshowMock.getSlides()).thenReturn(new ArrayList<SlideParserDTO>());
		var target = new CreatePresentationElementsVisitor(mock(PresentationElementCache.class), factoryMock);
		
		// Act
		target.visit(slideshowMock);
		
		// Assert
		verify(factoryMock, times(1)).createSlideshow(eq(slideshowMock), anyList());
    }
	
	
	@Test
    public void visit_slide_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationElementFactory.class);
		var slideMock = mock(SlideParserDTO.class);
		var target = new CreatePresentationElementsVisitor(mock(PresentationElementCache.class), factoryMock);
		
		// Act
		target.visit(slideMock);
		
		// Assert
		verify(factoryMock, times(1)).createSlide(eq(slideMock), anyList());
    }
	
	@Test
    public void visit_slideComponent_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationElementFactory.class);
		var slideComponentMock = mock(SlideComponentParserDTO.class);
		when(slideComponentMock.getChildren()).thenReturn(mock(SlideComponentParserDTO.Children.class));
		var levels = new HashMap<PresentationParserDTO, Integer>();
		levels.put(slideComponentMock, 0);
		var target = new CreatePresentationElementsVisitor(mock(PresentationElementCache.class), factoryMock, levels);

		// Act
		target.visit(slideComponentMock);
		
		// Assert
		verify(factoryMock, times(1)).createSlideComponent(eq(slideComponentMock), eq(0), anyList());
    }
	
	@Test
    public void visit_textitem_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationElementFactory.class);
		var textItemMock = mock(TextItemParserDTO.class);
		var target = new CreatePresentationElementsVisitor(mock(PresentationElementCache.class), factoryMock);
		
		// Act
		target.visit(textItemMock);
		
		// Assert
		verify(factoryMock, times(1)).createTextItem(eq(textItemMock));
    }
	
	@Test
    public void visit_titleitem_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationElementFactory.class);
		var titleItemMock = mock(TitleItemParserDTO.class);
		var target = new CreatePresentationElementsVisitor(mock(PresentationElementCache.class), factoryMock);
		
		// Act
		target.visit(titleItemMock);
		
		// Assert
		verify(factoryMock, times(1)).createTitleItem(eq(titleItemMock));
    }
	
	@Test
    public void visit_tableitem_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationElementFactory.class);
		var tableItemMock = mock(TableItemParserDTO.class);
		var target = new CreatePresentationElementsVisitor(mock(PresentationElementCache.class), factoryMock);
		
		// Act
		target.visit(tableItemMock);
		
		// Assert
		verify(factoryMock, times(1)).createTableItem(eq(tableItemMock));
    }
	
	@Test
    public void visit_mediaitem_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationElementFactory.class);
		var mediaItemMock = mock(MediaItemParserDTO.class);
		var target = new CreatePresentationElementsVisitor(mock(PresentationElementCache.class), factoryMock);
		
		// Act
		target.visit(mediaItemMock);
		
		// Assert
		verify(factoryMock, times(1)).createMediaItem(eq(mediaItemMock));
    }
}
