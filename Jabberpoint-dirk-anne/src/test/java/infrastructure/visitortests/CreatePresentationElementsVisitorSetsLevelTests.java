package infrastructure.visitortests;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.*;

import domain.factories.PresentationElementFactory;
import domain.repositories.PresentationElementCache;
import infrastructure.CreatePresentationElementsVisitor;
import infrastructure.parsermodels.SlideComponentParserDTO;
import infrastructure.parsermodels.SlideComponentParserDTO.Children;
import infrastructure.parsermodels.SlideParserDTO;
import infrastructure.parsermodels.SlideshowParserDTO;

public class CreatePresentationElementsVisitorSetsLevelTests {

	@Test
    public void visitor_sets_correct_levels() {
		// Arrange
		var factoryMock = mock(PresentationElementFactory.class);
		var slideshowDTO = new SlideshowParserDTO();
		var slideDTO1 = new SlideParserDTO();
		var component1Slide1 = new SlideComponentParserDTO();
		component1Slide1.setChildren(new Children());
		component1Slide1.getChildren().getComponents().add(new SlideComponentParserDTO());
		
		var child2Component1 = new SlideComponentParserDTO();
		child2Component1.setChildren(new Children());
		child2Component1.getChildren().getComponents().add(new SlideComponentParserDTO());
		component1Slide1.getChildren().getComponents().add(child2Component1);
		component1Slide1.getChildren().getComponents().add(new SlideComponentParserDTO());
		
		slideDTO1.getComponents().add(component1Slide1);
		slideDTO1.getComponents().add(new SlideComponentParserDTO());
		
		var slideDTO2 = new SlideParserDTO();
		slideDTO2.getComponents().add(new SlideComponentParserDTO());
		slideDTO2.getComponents().add(new SlideComponentParserDTO());
		slideDTO2.getComponents().add(new SlideComponentParserDTO());
		
		slideshowDTO.getSlides().add(slideDTO1);
		slideshowDTO.getSlides().add(slideDTO2);
		var target = new CreatePresentationElementsVisitor(mock(PresentationElementCache.class), factoryMock);
		
		// Act
		target.visit(slideshowDTO);
		
		// Assert: is called using correct level
		var level = 0;
		verify(factoryMock, times(5).description("There are four components at main level. Slide 1 has two, slide 2 has three."))
			.createSlideComponent(any(), eq(level), anyList());
		
		level = 1;
		verify(factoryMock, times(3).description("There are three components at first level"))
			.createSlideComponent(any(), eq(level), anyList());
		
		level = 2;
		verify(factoryMock, times(1).description("There is one components at second level"))
			.createSlideComponent(any(), eq(level), anyList());
    }
	
	
}
