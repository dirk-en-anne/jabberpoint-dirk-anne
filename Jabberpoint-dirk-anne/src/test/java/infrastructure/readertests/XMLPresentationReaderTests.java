package infrastructure.readertests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import org.junit.jupiter.api.*;

import infrastructure.PresentationParserException;
import infrastructure.XMLPresentationReader;
import infrastructure.parsermodels.MediaItemParserDTO;
import infrastructure.parsermodels.SlideshowParserDTO;
import infrastructure.parsermodels.TableItemParserDTO;
import infrastructure.parsermodels.TitleItemParserDTO;

public class XMLPresentationReaderTests {

	@Test
    public void Test() throws PresentationParserException, FileNotFoundException {
		// Arrange
		var target = new XMLPresentationReader();
		var inputstream = new FileInputStream(getClass().getResource("slideshow.xml").getPath());
		
		// Act
		var slideshows = (List<SlideshowParserDTO>)target.read(inputstream);
		
		// Assert
		assertEquals(2, slideshows.size(), "number of slideshows");
		var firstSlideshow = slideshows.get(0);
		var secondSlideshow = slideshows.get(1);
		assertEquals("Slideshow 1", firstSlideshow.getTitle(), "Title of first slideshow");
		assertEquals("Slideshow 2", secondSlideshow.getTitle(), "Title of second slideshow");
		
		var slidesFirstSlideshow = firstSlideshow.getSlides();
		assertEquals(2, slidesFirstSlideshow.size(), "number of slides first slideshow");
		var firstSlide = slidesFirstSlideshow.get(0);
		assertEquals("d62f4edd-10d3-4f2d-95b7-ca5c75525688", firstSlide.getId(), "id of first slide");
		var firstSlideFirstComponent = firstSlide.getComponents().get(0);
		assertEquals("822a7698-2a87-4c16-88fe-7b416fd64a20", firstSlideFirstComponent.getId(), "id of first component");
		var firstSlideFirstItem = (TitleItemParserDTO)firstSlideFirstComponent.getItem();
		assertEquals("This is Slideshow number 1", firstSlideFirstItem.getText(), "Test of first item");
		
		var secondSlideFirstSlideshow = slidesFirstSlideshow.get(1);
		var secondSlideSecondSlideshow = secondSlideshow.getSlides().get(1);
		assertEquals(secondSlideFirstSlideshow, secondSlideSecondSlideshow, "Second slide both slideshows is the same object");
		assertEquals(secondSlideFirstSlideshow.getId(), secondSlideSecondSlideshow.getId(), "Second slide both slideshows is the same id");
		
		var childerenComponentsFirstSlideshow = secondSlideFirstSlideshow.getComponents().get(0).getChildren().getComponents();
		var childerenComponentsSecondSlideshow = secondSlideSecondSlideshow.getComponents().get(0).getChildren().getComponents();
		
		var mediaItemFirstSlideshow = (MediaItemParserDTO)childerenComponentsFirstSlideshow.get(0).getItem();
		var mediaItemSecondSlideshow = (MediaItemParserDTO)childerenComponentsSecondSlideshow.get(0).getItem();
		assertEquals("img1.jpg", mediaItemFirstSlideshow.getSource(), "Should be an image");
		assertEquals(mediaItemFirstSlideshow.getSource(), mediaItemSecondSlideshow.getSource(), "Both slideshows should show the same image");
		
		var tableFirstSlideshow = (TableItemParserDTO)childerenComponentsFirstSlideshow.get(2).getItem();
		var tableSecondSlideshow = (TableItemParserDTO)childerenComponentsSecondSlideshow.get(2).getItem();
		assertEquals("Apples", tableFirstSlideshow.getRows().get(0).getColumns().get(0), "Should be Apples");
		assertEquals("Bananas", tableFirstSlideshow.getRows().get(0).getColumns().get(1), "Should be Bananas");
		assertEquals(tableFirstSlideshow.getRows().get(0).getColumns().get(0), tableSecondSlideshow.getRows().get(0).getColumns().get(0), "Both slideshows should show Apples");
		assertEquals(tableFirstSlideshow.getRows().get(0).getColumns().get(1), tableSecondSlideshow.getRows().get(0).getColumns().get(1), "Both slideshows should show Bananas");
    }
}
