package infrastructure.writertests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import javax.xml.bind.JAXBException;

import org.junit.jupiter.api.*;

import domain.factories.DefaultPresentationParserDTOFactory;
import domain.entities.*;
import domain.entities.slideItems.*;
import domain.visitors.CreatePresentationParserDTOVisitor;
import infrastructure.XMLPresentationWriter;
import infrastructure.builders.DefaultJabberpointRootElementBuilder;

public class XMLPresentationWriterTests {
	@Test
    public void Test() throws UnsupportedEncodingException, JAXBException {
		// Arrange
		var slideshow1Slides = new ArrayList<PresentationElement>();
		var slide1 = new Slide("d62f4edd-10d3-4f2d-95b7-ca5c75525688", new ArrayList<PresentationElement>());
		var slide1component1 = new SlideComponent("822a7698-2a87-4c16-88fe-7b416fd64a20", 0, new ArrayList<PresentationElement>());
		slide1component1.getElements().add(new TitleItem("This is Slideshow number 1"));
		var slide1component1child1 = new SlideComponent("e0049245-7346-4952-b8ce-05e25b6f3331", 0, new ArrayList<PresentationElement>());
		
		var rows = new ArrayList<TableRow>();
		var columns = new ArrayList<String>();
		columns.add("Apples");
		columns.add("Bananas");
		rows.add(new TableRow(columns));
		var fruitTable = new TableItem(rows);
		
		slide1component1child1.getElements().add(fruitTable);
		slide1component1.getElements().add(slide1component1child1);
		slide1.getElements().add(slide1component1);
		slideshow1Slides.add(slide1);
		var slideshow1 = new Slideshow("slideshow 1", slideshow1Slides);
		
		var target = new XMLPresentationWriter(
			new CreatePresentationParserDTOVisitor(
				new DefaultPresentationParserDTOFactory(),
				new DefaultJabberpointRootElementBuilder()));
		
		var slideshows = new ArrayList<PresentationElement>();
		slideshows.add(slideshow1);
		var outputStream = new ByteArrayOutputStream();
		
		// Act
		target.Write(outputStream, slideshows);
		
		// Assert
		var expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n" + 
				"<Jabberpoint>\r\n" + 
				"    <Slideshow>\r\n" + 
				"        <Title>slideshow 1</Title>\r\n" + 
				"        <Slide>d62f4edd-10d3-4f2d-95b7-ca5c75525688</Slide>\r\n" + 
				"    </Slideshow>\r\n" + 
				"    <Slide>\r\n" + 
				"        <Id>d62f4edd-10d3-4f2d-95b7-ca5c75525688</Id>\r\n" + 
				"        <Component>822a7698-2a87-4c16-88fe-7b416fd64a20</Component>\r\n" + 
				"    </Slide>\r\n" + 
				"    <Component>\r\n" + 
				"        <Id>e0049245-7346-4952-b8ce-05e25b6f3331</Id>\r\n" + 
				"        <Item xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"Table\">\r\n" + 
				"            <tr>\r\n" + 
				"                <td>Apples</td>\r\n" + 
				"                <td>Bananas</td>\r\n" + 
				"            </tr>\r\n" + 
				"        </Item>\r\n" + 
				"        <Children/>\r\n" + 
				"    </Component>\r\n" + 
				"    <Component>\r\n" + 
				"        <Id>822a7698-2a87-4c16-88fe-7b416fd64a20</Id>\r\n" + 
				"        <Item xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"Title\">\r\n" + 
				"            <Text>This is Slideshow number 1</Text>\r\n" + 
				"        </Item>\r\n" + 
				"        <Children>\r\n" + 
				"            <Component>e0049245-7346-4952-b8ce-05e25b6f3331</Component>\r\n" + 
				"        </Children>\r\n" + 
				"    </Component>\r\n" + 
				"</Jabberpoint>\r\n" + 
				"";
		assertEquals(expected.replaceAll("\\s+",""), outputStream.toString("UTF-8").replaceAll("\\s+",""));
	}
}
