package domain.visitortests;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.*;

import domain.factories.PresentationParserDTOFactory;
import domain.entities.*;
import domain.entities.slideItems.*;
import domain.visitors.CreatePresentationParserDTOVisitor;
import infrastructure.builders.JabberpointRootElementBuilder;

public class CreatePresentationParserDTOVisitorCallsFactoryTests {

	@Test
    public void visit_slideshow_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationParserDTOFactory.class);
		var slideshowMock = mock(Slideshow.class);
		when(slideshowMock.getElements()).thenReturn(new ArrayList<PresentationElement>());
		var target = new CreatePresentationParserDTOVisitor(factoryMock, mock(JabberpointRootElementBuilder.class));
		
		// Act
		target.visit(slideshowMock);
		
		// Assert
		verify(factoryMock, times(1)).createSlideshow(eq(slideshowMock), anyList());
    }
	
	@Test
    public void visit_slide_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationParserDTOFactory.class);
		var slideMock = mock(Slide.class);
		var target = new CreatePresentationParserDTOVisitor(factoryMock, mock(JabberpointRootElementBuilder.class));
		
		// Act
		target.visit(slideMock);
		
		// Assert
		verify(factoryMock, times(1)).createSlide(eq(slideMock), anyList());
    }
	
	@Test
    public void visit_slideComponent_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationParserDTOFactory.class);
		var slideComponentMock = mock(SlideComponent.class);
		var target = new CreatePresentationParserDTOVisitor(factoryMock, mock(JabberpointRootElementBuilder.class));

		// Act
		target.visit(slideComponentMock);
		
		// Assert
		verify(factoryMock, times(1)).createSlideComponent(eq(slideComponentMock), anyList());
    }
	
	@Test
    public void visit_textitem_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationParserDTOFactory.class);
		var textItemMock = mock(TextItem.class);
		var target = new CreatePresentationParserDTOVisitor(factoryMock, mock(JabberpointRootElementBuilder.class));
		
		// Act
		target.visit(textItemMock);
		
		// Assert
		verify(factoryMock, times(1)).createTextItem(eq(textItemMock));
    }
	
	@Test
    public void visit_titleitem_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationParserDTOFactory.class);
		var titleItemMock = mock(TitleItem.class);
		var target = new CreatePresentationParserDTOVisitor(factoryMock, mock(JabberpointRootElementBuilder.class));
		
		// Act
		target.visit(titleItemMock);
		
		// Assert
		verify(factoryMock, times(1)).createTitleItem(eq(titleItemMock));
    }
	
	@Test
    public void visit_tableitem_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationParserDTOFactory.class);
		var tableItemMock = mock(TableItem.class);
		var target = new CreatePresentationParserDTOVisitor(factoryMock, mock(JabberpointRootElementBuilder.class));
		
		// Act
		target.visit(tableItemMock);
		
		// Assert
		verify(factoryMock, times(1)).createTableItem(eq(tableItemMock));
    }
	
	@Test
    public void visit_mediaitem_calls_factory() {
		// Arrange
		var factoryMock = mock(PresentationParserDTOFactory.class);
		var mediaItemMock = mock(MediaItem.class);
		var target = new CreatePresentationParserDTOVisitor(factoryMock, mock(JabberpointRootElementBuilder.class));
		
		// Act
		target.visit(mediaItemMock);
		
		// Assert
		verify(factoryMock, times(1)).createMediaItem(eq(mediaItemMock));
    }
}
