package domain.factorytests;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.*;

import domain.factories.DefaultPresentationElementFactory;
import domain.entities.*;
import infrastructure.parsermodels.*;
import uk.co.jemos.podam.api.PodamFactoryImpl;

/**
 * 
 * These tests use @see PodamFactoryImpl to generate an object and filling it with random fields. 
 * These tests also use assertj's {@link #isEqualToIgnoringGivenFields(Object, String...)} to compare all fields of two different classes.
 * This way, these tests will fail automatically if a new field is added, but not set by the {@link DefaultPresentationElementFactory}
 *
 */
public class DefaultPresentationElementFactoryMappingTests {

	@Test
    public void createSlideshow_Maps_all_Fields() {
		// Arrange
		var target = new DefaultPresentationElementFactory();
		var factory = new PodamFactoryImpl();
		var slideshowDTO = factory.manufacturePojo(SlideshowParserDTO.class);
		
		// Act
		var slideshow = target.createSlideshow(slideshowDTO, new ArrayList<PresentationElement>());
		
		// Assert
		assertThat(slideshow).isEqualToIgnoringGivenFields(slideshowDTO, "elements", "level");
    }
	
	@Test
    public void createSlide_Maps_all_Fields() {
		// Arrange
		var target = new DefaultPresentationElementFactory();
		var factory = new PodamFactoryImpl();
		var slideDTO = factory.manufacturePojo(SlideParserDTO.class);
		
		// Act
		var slide = target.createSlide(slideDTO, new ArrayList<PresentationElement>());
		
		// Assert
		assertThat(slide).isEqualToIgnoringGivenFields(slideDTO, "elements", "level");
    }
	
	@Test
    public void createSlideComponent_Sets_Level() {
		// Arrange
		var target = new DefaultPresentationElementFactory();
		var factory = new PodamFactoryImpl();
		var slideComponentDTO = factory.manufacturePojo(SlideComponentParserDTO.class);
		
		// Act
		var slideComponent = target.createSlideComponent(slideComponentDTO, 5, new ArrayList<PresentationElement>());
		
		// Assert
		assertEquals(5, slideComponent.getLevel());
    }
	
	@Test
    public void createSlideComponent_Maps_all_Fields() {
		// Arrange
		var target = new DefaultPresentationElementFactory();
		var factory = new PodamFactoryImpl();
		var slideComponentDTO = factory.manufacturePojo(SlideComponentParserDTO.class);
		
		// Act
		var slideComponent = target.createSlideComponent(slideComponentDTO, 0, new ArrayList<PresentationElement>());
		
		// Assert
		assertThat(slideComponent).isEqualToIgnoringGivenFields(slideComponentDTO, "elements", "level");
    }
	
	@Test
    public void createTitleItem_Maps_all_Fields() {
		// Arrange
		var target = new DefaultPresentationElementFactory();
		var factory = new PodamFactoryImpl();
		var titleItemDTO = factory.manufacturePojo(TitleItemParserDTO.class);
		
		// Act
		var titleItem = target.createTitleItem(titleItemDTO);
		
		// Assert
		assertThat(titleItem).isEqualToIgnoringGivenFields(titleItemDTO, "elements", "level");
    }
	
	@Test
    public void createTextItem_Maps_all_Fields() {
		// Arrange
		var target = new DefaultPresentationElementFactory();
		var factory = new PodamFactoryImpl();
		var textItemDTO = factory.manufacturePojo(TextItemParserDTO.class);
		
		// Act
		var textItem = target.createTextItem(textItemDTO);
		
		// Assert
		assertThat(textItem).isEqualToIgnoringGivenFields(textItemDTO, "elements", "level");
    }
	
	@Test
    public void createMediaItem_Maps_all_Fields() {		
		// Arrange
		var target = new DefaultPresentationElementFactory();
		var factory = new PodamFactoryImpl();
		var mediaItemDTO = factory.manufacturePojo(MediaItemParserDTO.class);
		
		// Act
		var mediaItem = target.createMediaItem(mediaItemDTO);
		
		// Assert
		assertThat(mediaItem).isEqualToIgnoringGivenFields(mediaItemDTO, "elements", "level");
    }
	
	@Test
    public void createTableItem_Maps_all_Fields() {
		// Arrange
		var target = new DefaultPresentationElementFactory();
		var factory = new PodamFactoryImpl();
		var tableItemDTO = factory.manufacturePojo(TableItemParserDTO.class);
		
		// Act
		var tableItem = target.createTableItem(tableItemDTO);
		
		// Assert
		assertThat(tableItem).isEqualToIgnoringGivenFields(tableItemDTO, "elements", "level", "rows");
    }
}
