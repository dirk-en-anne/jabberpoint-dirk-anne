package domain.factorytests;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import org.junit.jupiter.api.*;

import domain.entities.*;
import domain.entities.slideItems.*;
import domain.factories.DefaultPresentationParserDTOFactory;
import infrastructure.parsermodels.*;
import uk.co.jemos.podam.api.PodamFactoryImpl;

/**
 * 
 * These tests use @see PodamFactoryImpl to generate an object and filling it with random fields. 
 * These tests also use assertj's {@link #isEqualToIgnoringGivenFields(Object, String...)} to compare all fields of two different classes.
 * This way, these tests will fail automatically if a new field is added, but not set by the {@link DefaultPresentationParserDTOFactory}
 *
 */
public class DefaultPresentationParserDTOFactoryMappingTests {

	@Test
    public void createSlideshow_Maps_all_Fields() {
		// Arrange
		var target = new DefaultPresentationParserDTOFactory();
		var factory = new PodamFactoryImpl();
		var slideshow = factory.manufacturePojo(Slideshow.class);
		
		// Act
		var slideshowDTO = target.createSlideshow(slideshow, new ArrayList<SlideParserDTO>());
		
		// Assert
		assertThat(slideshowDTO).isEqualToIgnoringGivenFields(slideshow, "slides");
    }
	
	@Test
    public void createSlide_Maps_all_Fields() {
		// Arrange
		var target = new DefaultPresentationParserDTOFactory();
		var factory = new PodamFactoryImpl();
		var slide = factory.manufacturePojo(Slide.class);
		
		// Act
		var slideDTO = target.createSlide(slide, new ArrayList<SlideComponentParserDTO>());
		
		// Assert
		assertThat(slideDTO).isEqualToIgnoringGivenFields(slide, "components");
    }
	
	@Test
    public void createSlideComponent_Maps_all_Fields() {
		// Arrange
		var target = new DefaultPresentationParserDTOFactory();
		var factory = new PodamFactoryImpl();
		var slideComponent = factory.manufacturePojo(SlideComponent.class);
		
		// Act
		var slideComponentDTO = target.createSlideComponent(slideComponent, new ArrayList<PresentationParserDTO>());
		
		// Assert
		assertThat(slideComponentDTO).isEqualToIgnoringGivenFields(slideComponent, "children", "item");
    }
	
	@Test
    public void createTitleItem_Maps_all_Fields() {
		// Arrange
		var target = new DefaultPresentationParserDTOFactory();
		var factory = new PodamFactoryImpl();
		var titleItem = factory.manufacturePojo(TitleItem.class);
		
		// Act
		var titleItemDTO = target.createTitleItem(titleItem);
		
		// Assert
		assertThat(titleItemDTO).isEqualToIgnoringGivenFields(titleItem);
    }
	
	@Test
    public void createTextItem_Maps_all_Fields() {
		// Arrange
		var target = new DefaultPresentationParserDTOFactory();
		var factory = new PodamFactoryImpl();
		var textItem = factory.manufacturePojo(TextItem.class);
		
		// Act
		var textItemDTO = target.createTextItem(textItem);
		
		// Assert
		assertThat(textItemDTO).isEqualToIgnoringGivenFields(textItem);
    }
	
	@Test
    public void createMediaItem_Maps_all_Fields() {		
		// Arrange
		var target = new DefaultPresentationParserDTOFactory();
		var factory = new PodamFactoryImpl();
		var mediaItem = factory.manufacturePojo(MediaItem.class);
		
		// Act
		var mediaItemDTO = target.createMediaItem(mediaItem);
		
		// Assert
		assertThat(mediaItemDTO).isEqualToIgnoringGivenFields(mediaItem);
    }
	
	@Test
    public void createTableItem_Maps_all_Fields() {
		// Arrange
		var target = new DefaultPresentationParserDTOFactory();
		var factory = new PodamFactoryImpl();
		var tableItem = factory.manufacturePojo(TableItem.class);
		
		// Act
		var tableItemDTO = target.createTableItem(tableItem);
		
		// Assert
		assertThat(tableItemDTO).isEqualToIgnoringGivenFields(tableItem, "rows");
    }
}
