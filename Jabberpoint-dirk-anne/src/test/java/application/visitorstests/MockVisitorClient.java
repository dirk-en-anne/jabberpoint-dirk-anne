package application.visitorstests;

import java.util.List;

import domain.entities.*;
import domain.entities.slideItems.*;
import domain.visitors.*;
import presentation.visitors.*;
import presentation.visitors.builders.DefaultVisitorBuilder;

public class MockVisitorClient extends AbstractVisitorClient<PresentationElement> 
{
	public MockVisitorClient(List<String> tracker)
	{
		super(visitor -> createVisitor(visitor, tracker));
	}
	
	private static GenericVisitor<PresentationElement> createVisitor(AbstractVisitorClient<PresentationElement> visitor, List<String> tracker) 
	{	
		return DefaultVisitorBuilder
			.startWith(new DisplaySlideVisitor(visitor))
			.attach(new DisplaySlideComponentVisitor(visitor))
			.attach(new DisplaySlideItemVisitor<TextItem>(TextItem.class, visitor, new MockDisplayAlgorithm<TextItem>(tracker, "TextItem")))
			.attach(new DisplaySlideItemVisitor<MediaItem>(MediaItem.class, visitor, new MockDisplayAlgorithm<MediaItem>(tracker, "MediaItem")))
			.build();
	}
}
