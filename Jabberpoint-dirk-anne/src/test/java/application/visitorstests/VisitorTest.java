package application.visitorstests;

import java.util.ArrayList;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Assertions;
import domain.entities.*;
import domain.entities.slideItems.*;

public class VisitorTest 
{
	@DisplayName("Test visitor setup & workings")
	@Test
	public void TestVisitorWorkings() 
	{
		// Arrange
		var tracker = new ArrayList<String>();
		var visitor = new MockVisitorClient(tracker);
		
		var items = new ArrayList<PresentationElement>();
		items.add(new TextItem("Text 1"));
		items.add(new TextItem("Text 2"));
		items.add(new MediaItem("image.png"));
		items.add(new TableItem(new ArrayList<TableRow>()));
		
		var components = new ArrayList<PresentationElement>();
		components.add(new SlideComponent("68d6cbf0-ff2d-4d7c-97e2-2ccf1f8e75ee", 1, items));
		
		var model = new Slide("68d6cbf0-ff2d-4d7c-97e2-2ccf1f8e75ee", components);
		
		// Act
		model.display(visitor);
		
		// Assert
		Assertions.assertEquals(4, tracker.size());
		
		var expectations = new String[] {
			"Found: Slide",
			"Found: TextItem",
			"Found: TextItem",
			"Found: MediaItem"
		};
		
		for (var index = 0; index < tracker.size(); index++)
		{
			Assertions.assertEquals(expectations[index], tracker.get(index));
		}
	}
}
