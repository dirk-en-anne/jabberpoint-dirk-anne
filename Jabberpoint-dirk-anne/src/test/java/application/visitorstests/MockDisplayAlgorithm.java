package application.visitorstests;

import java.awt.Graphics;
import java.util.List;

import application.algorithms.DisplayAlgorithm;
import domain.entities.PresentationElement;

public class MockDisplayAlgorithm<TElement extends PresentationElement> implements DisplayAlgorithm<TElement> 
{
	private List<String> tracker;
	private final String typeString;
	
	public MockDisplayAlgorithm(List<String> tracker, String typeString)
	{
		this.tracker = tracker;
		this.typeString = typeString;
	}
	
	@Override
	public void display(Graphics graphics, TElement element) 
	{
		tracker.add("Found: " + typeString);
	}
}
