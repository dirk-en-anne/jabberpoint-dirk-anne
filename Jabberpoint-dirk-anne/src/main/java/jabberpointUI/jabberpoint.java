package jabberpointUI;

import java.awt.EventQueue;

import javax.swing.JComponent;

import application.*;
import domain.entities.PresentationElement;
import presentation.factories.*;
import presentation.userinterface.configuration.DefaultStyleProvider;
import presentation.userinterface.configuration.options.*;
import presentation.userinterface.frames.*;
import presentation.visitors.AbstractVisitorClient;

public class jabberpoint {

	public static SlideViewerFrame frame;
	public static SlideViewerFrameComponent component;
	public static Options<DisplayOptionsValue> options;
	public static StyleProvider styleProvider;
	
	private static PresentationServices presentation;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new jabberpoint();
					frame.setVisible(true);
					frame.repaint();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public jabberpoint() 
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private static void initialize() 
	{
		styleProvider = new DefaultStyleProvider();
		options = new DisplayOptions(new DisplayOptionsValue(1.f, 5.f, 100, 1200, 800));
		
		frame = new SlideViewerFrame(new AWTMenuBuilderFactory(), options);
		var visitorFactory = new DefaultVisitorFactory(new DefaultAlgorithmsFactory());
		
		var displayVisitor = (AbstractVisitorClient<PresentationElement>)visitorFactory.createDisplayVisitor();
		component = new SlideViewerFrameComponent(options, displayVisitor);
		
		var configuration = new DefaultConfiguration(visitorFactory);
		presentation = new DefaultPresentationFactory(configuration).CreatePresentationService(options);
		
		frame.setBounds(100, 100, 1200, 800);
		frame.setupWindow(component, presentation);
	}
}
