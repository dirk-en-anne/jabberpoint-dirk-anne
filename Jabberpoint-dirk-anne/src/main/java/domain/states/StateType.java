package domain.states;

public enum StateType 
{
	Unloaded,
	Loaded,
	Displaying,
	Ended
}
