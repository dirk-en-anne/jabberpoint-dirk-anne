package domain.states;

import domain.entities.PresentationElement;

public interface PresentationState 
{
	void start();
	void load();
	void next();
	void previous();
	void unload();
	void stop();
	StateType getType();
}