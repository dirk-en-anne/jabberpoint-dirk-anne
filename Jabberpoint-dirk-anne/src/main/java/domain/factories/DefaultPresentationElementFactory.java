package domain.factories;

import java.util.ArrayList;
import java.util.List;

import domain.entities.*;
import domain.entities.slideItems.*;
import infrastructure.parsermodels.*;

public class DefaultPresentationElementFactory implements PresentationElementFactory {

	public PresentationElement createSlideshow(SlideshowParserDTO slideshowDTO, List<PresentationElement> slides) 
	{
		return new Slideshow(slideshowDTO.getTitle(), slides);
	}

	public PresentationElement createSlide(SlideParserDTO slideDTO, List<PresentationElement> components) 
	{
		var slide = new Slide(slideDTO.getId(), components);
		return slide;
	}

	public PresentationElement createSlideComponent(SlideComponentParserDTO slideComponentDTO, int level, List<PresentationElement> children) 
	{
		return new SlideComponent(slideComponentDTO.getId(), level, children);
	}

	public PresentationElement createTableItem(TableItemParserDTO tableItemDTO) 
	{
		var rows = new ArrayList<TableRow>();
		for(var rowDTO : tableItemDTO.getRows()) {
			rows.add(new TableRow(rowDTO.getColumns()));
		}
		return new TableItem(rows);
	}

	public PresentationElement createMediaItem(MediaItemParserDTO mediaItemDTO) 
	{
		return new MediaItem(mediaItemDTO.getSource());
	}

	public PresentationElement createTitleItem(TitleItemParserDTO titleItemDTO) 
	{
		return new TitleItem(titleItemDTO.getText());
	}

	public PresentationElement createTextItem(TextItemParserDTO textItemDTO) 
	{
		return new TextItem(textItemDTO.getText());
	}

	public PresentationElement createNoItem() 
	{
		return new NoItem();
	}
}
