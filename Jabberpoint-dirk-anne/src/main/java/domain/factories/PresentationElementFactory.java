package domain.factories;

import java.util.List;

import domain.entities.PresentationElement;
import infrastructure.parsermodels.*;

public interface PresentationElementFactory {

	PresentationElement createSlideshow(SlideshowParserDTO slideshow, List<PresentationElement> slides);
	PresentationElement createSlide(SlideParserDTO slide, List<PresentationElement> components);
	PresentationElement createSlideComponent(SlideComponentParserDTO slideComponent, int level, List<PresentationElement> children);
	PresentationElement createTableItem(TableItemParserDTO tableItemDTO);
	PresentationElement createMediaItem(MediaItemParserDTO mediaItemDTO);
	PresentationElement createTitleItem(TitleItemParserDTO titleItemDTO);
	PresentationElement createTextItem(TextItemParserDTO textItemDTO);
	PresentationElement createNoItem();

}
