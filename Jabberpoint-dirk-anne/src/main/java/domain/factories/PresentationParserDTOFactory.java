package domain.factories;

import java.util.List;

import domain.entities.*;
import domain.entities.slideItems.*;
import infrastructure.parsermodels.*;

public interface PresentationParserDTOFactory {
	PresentationParserDTO createSlideshow(Slideshow slideshow, List<SlideParserDTO> slides);

	PresentationParserDTO createSlide(Slide slide, List<SlideComponentParserDTO> components);

	PresentationParserDTO createSlideComponent(SlideComponent slideComponent, List<PresentationParserDTO> children);

	PresentationParserDTO createTableItem(TableItem tableItem);

	PresentationParserDTO createMediaItem(MediaItem mediaItem);

	PresentationParserDTO createTitleItem(TitleItem titleItem);

	PresentationParserDTO createTextItem(TextItem textItem);

	PresentationParserDTO createNoItem(NoItem noItem);
}
