package domain.factories;

import java.util.ArrayList;
import java.util.List;

import domain.entities.*;
import domain.entities.slideItems.*;
import infrastructure.parsermodels.*;
import infrastructure.parsermodels.SlideComponentParserDTO.Children;

public class DefaultPresentationParserDTOFactory implements PresentationParserDTOFactory {

	@Override
	public PresentationParserDTO createSlideshow(Slideshow slideshow, List<SlideParserDTO> slides) {
		var slideshowDTO = new SlideshowParserDTO();
		slideshowDTO.setSlides(slides);
		slideshowDTO.setTitle(slideshow.getTitle());
		return slideshowDTO;
	}

	@Override
	public PresentationParserDTO createSlide(Slide slide, List<SlideComponentParserDTO> components) {
		var slideDTO = new SlideParserDTO();
		slideDTO.setComponents(components);
		slideDTO.setId(slide.getId());
		return slideDTO;
	}

	@Override
	public PresentationParserDTO createSlideComponent(SlideComponent slideComponent, List<PresentationParserDTO> elements) {
		var slideComponentDTO = new SlideComponentParserDTO();
		slideComponentDTO.setChildren(new Children());
		var children = new ArrayList<SlideComponentParserDTO>();
		SlideItemParserDTO slideItemParserDTO = null;
		for (var element : elements) {
			if(element instanceof SlideItemParserDTO) {
				slideItemParserDTO = (SlideItemParserDTO)element; 
			}
			if(element instanceof SlideComponentParserDTO) {
				children.add((SlideComponentParserDTO) element);
			}
		}
		
		slideComponentDTO.setItem(slideItemParserDTO);
		slideComponentDTO.getChildren().setComponents(children);
		slideComponentDTO.setId(slideComponent.getId());
		return slideComponentDTO;
	}

	@Override
	public PresentationParserDTO createTableItem(TableItem tableItem) {
		var tableItemDTO = new TableItemParserDTO();
		var rowDTOs = new ArrayList<TableRowParserDTO>(0);
		for (var row : tableItem.getRows()) {
			var rowDTO = new TableRowParserDTO();
			rowDTO.setColumns(row.getColumns());
			rowDTOs.add(rowDTO);
		}
		tableItemDTO.setRows(rowDTOs);
		return tableItemDTO;
	}

	@Override
	public PresentationParserDTO createMediaItem(MediaItem mediaItem) {
		var mediaItemDTO = new MediaItemParserDTO();
		mediaItemDTO.setSource(mediaItem.getSource());
		return mediaItemDTO;
	}

	@Override
	public PresentationParserDTO createTitleItem(TitleItem titleItem) {
		var titleItemDTO = new TitleItemParserDTO();
		titleItemDTO.setText(titleItem.getText());
		return titleItemDTO;
	}

	@Override
	public PresentationParserDTO createTextItem(TextItem textItem) {
		var textItemDTO = new TextItemParserDTO();
		textItemDTO.setText(textItem.getText());
		return textItemDTO;
	}

	@Override
	public PresentationParserDTO createNoItem(NoItem noItem) {
		return null;
	}

}
