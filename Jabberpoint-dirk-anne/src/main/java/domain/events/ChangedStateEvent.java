package domain.events;

import domain.states.StateType;

public class ChangedStateEvent 
{
	private StateType stateType;
	
	public ChangedStateEvent(StateType stateType)
	{
		this.stateType = stateType;
	}
	
	public StateType getStateType()
	{
		return stateType;
	}
}
