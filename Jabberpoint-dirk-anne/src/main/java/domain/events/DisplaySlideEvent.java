package domain.events;

import domain.entities.PresentationElement;

public class DisplaySlideEvent
{
	private PresentationElement slide;
	
	public DisplaySlideEvent(PresentationElement slide)
	{
		this.slide = slide;
	}
	
	public PresentationElement getSlide() 
	{
		return slide;
	}
}
