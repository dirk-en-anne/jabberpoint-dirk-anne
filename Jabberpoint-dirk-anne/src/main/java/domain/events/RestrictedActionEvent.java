package domain.events;

public class RestrictedActionEvent 
{
	private String actionName;
	private String reason;
	
	public RestrictedActionEvent(String actionName, String reason) 
	{
		this.actionName = actionName;
		this.reason = reason;
	}
	
	public String getName() 
	{
		return actionName;
	}
	
	public String getReason()
	{
		return reason;
	}
}
