package domain.events;

import domain.events.EventListener;

public interface EventDispatcher<TEvent>
{
	void addListener(EventListener<TEvent> listener);
	void removeListener(EventListener<TEvent> listener);
	void createEvent(TEvent event);
}