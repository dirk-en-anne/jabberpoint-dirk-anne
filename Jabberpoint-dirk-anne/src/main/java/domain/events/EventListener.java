package domain.events;

/*
 * The interface for the event listeners. It extends the java:util:EventListener interface, 
 * that functions as a tag, to indicate this is regarded as a decent event listener.
 * */
public interface EventListener<TEvent> extends java.util.EventListener
{
	void onEvent(TEvent event);
}
