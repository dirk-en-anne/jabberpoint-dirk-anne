package domain.events;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractEventDispatcher<TEvent> implements EventDispatcher<TEvent> 
{
	protected List<EventListener<TEvent>> listeners;
	
	protected AbstractEventDispatcher()
	{
		listeners = new ArrayList<EventListener<TEvent>>();
	}
	
	protected abstract void fireEvent(TEvent event);
	
	public void addListener(EventListener<TEvent> listener)
	{
		if (!listeners.contains(listener))
		{
			listeners.add(listener);
		}
	}
	
	public void removeListener(EventListener<TEvent> listener)
	{
		listeners.remove(listener);
	}
	
	public void createEvent(TEvent event)
	{
		fireEvent(event);
	}
}
