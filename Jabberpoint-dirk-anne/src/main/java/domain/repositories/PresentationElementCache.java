package domain.repositories;

import domain.entities.PresentationElement;

/**
 * Repository
 */
public interface PresentationElementCache {
	PresentationElement GetPresentationElement(String id);
	void SetPresentationElement(String id, PresentationElement presentationElement);
}
