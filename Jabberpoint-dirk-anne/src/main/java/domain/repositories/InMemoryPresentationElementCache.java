package domain.repositories;

import java.util.HashMap;
import java.util.Map;

import domain.entities.PresentationElement;

public class InMemoryPresentationElementCache implements PresentationElementCache {
	private final Map<String, PresentationElement> presentationElements;
	
	public InMemoryPresentationElementCache() {
		presentationElements = new HashMap<String, PresentationElement>();
	}

	@Override
	public PresentationElement GetPresentationElement(String id) {
		return presentationElements.get(id);
	}

	@Override
	public void SetPresentationElement(String id, PresentationElement presentationElement) {
		presentationElements.put(id, presentationElement);
	}
	
	
}
