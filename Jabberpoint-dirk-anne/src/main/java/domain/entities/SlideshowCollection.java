package domain.entities;

import java.util.List;

import domain.iterators.DefaultPresentationIterator;
import domain.iterators.PresentationElementIterator;
import domain.visitors.MapPresentationElementToDTOVisitor;
import domain.visitors.VisitorClient;

public class SlideshowCollection extends PresentationElement 
{
	public SlideshowCollection(List<PresentationElement> elements) 
	{
		super(elements);
	}

	@Override
	public void display(VisitorClient<PresentationElement> visitor) { }

	@Override
	public <TDTO> TDTO accept(MapPresentationElementToDTOVisitor<TDTO> visitor) 
	{
		return null;
	}

	@Override
	public PresentationElementIterator getIterator() 
	{
		return new DefaultPresentationIterator(elements);
	}

}
