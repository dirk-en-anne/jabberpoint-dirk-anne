package domain.entities;

import java.util.List;

import domain.iterators.DefaultPresentationIterator;
import domain.iterators.PresentationElementIterator;
import domain.visitors.*;

public class SlideComponent extends PresentationElement 
{
	private final String id;

	public SlideComponent(String id, int level, List<PresentationElement> slideItems) 
	{
		super(slideItems);
		this.id = id;
		this.level = level;
	}
	
	public String getId() {
		return id;
	}

	@Override
	public void display(VisitorClient<PresentationElement> visitor)
	{
		visitor.visit(this);
	}

	@Override
	public <TDTO> TDTO accept(MapPresentationElementToDTOVisitor<TDTO> visitor)
	{
		return visitor.visit(this);
	}
	
	@Override
	public PresentationElementIterator getIterator()
	{
		return new DefaultPresentationIterator(elements);
	}
}
