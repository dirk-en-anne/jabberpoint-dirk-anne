package domain.entities;

import java.util.List;

import domain.iterators.PresentationElementIterator;
import domain.visitors.*;

public abstract class PresentationElement 
{
	protected final List<PresentationElement> elements;
	protected int level;
	
	protected PresentationElement(List<PresentationElement> elements) 
	{
		this.elements = elements;
	}
	
	public int getLevel() 
	{ 
		return level; 
	}
	
	public List<PresentationElement> getElements() 
	{
		return elements;
	}

	public abstract void display(VisitorClient<PresentationElement> visitor);
	public abstract <TDTO> TDTO accept(MapPresentationElementToDTOVisitor<TDTO> visitor);
	public abstract PresentationElementIterator getIterator();
}
