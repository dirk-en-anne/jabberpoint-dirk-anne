package domain.entities.slideItems;

import domain.iterators.*;
import domain.entities.PresentationElement;
import domain.visitors.*;

public class TitleItem extends LexicalItem 
{
	public TitleItem(String text) 
	{
		super(text);
	}
	
	@Override
	public void display(VisitorClient<PresentationElement> visitor)
	{
		visitor.visit(this);
	}

	@Override
	public <TDTO> TDTO accept(MapPresentationElementToDTOVisitor<TDTO> visitor)
	{
		return visitor.visit(this);
	}

	@Override
	public PresentationElementIterator getIterator()
	{
		return new DefaultPresentationIterator(elements);
	}
}
