package domain.entities.slideItems;

import domain.iterators.DefaultPresentationIterator;
import domain.iterators.PresentationElementIterator;
import domain.entities.PresentationElement;
import domain.visitors.*;

public class TextItem extends LexicalItem 
{
	public TextItem(String text)
	{
		super(text);
	}

	@Override
	public void display(VisitorClient<PresentationElement> visitor)
	{
		visitor.visit(this);
	}

	@Override
	public <TDTO> TDTO accept(MapPresentationElementToDTOVisitor<TDTO> visitor)
	{
		return visitor.visit(this);
	}

	@Override
	public PresentationElementIterator getIterator()
	{
		return new DefaultPresentationIterator(elements);
	}
}
