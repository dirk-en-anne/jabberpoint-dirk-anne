package domain.entities.slideItems;

import domain.entities.*;

public abstract class LexicalItem extends SlideItem 
{
	private String text;
	
	protected LexicalItem(String text) 
	{
		this.text = text;
	}
	
	public String getText()
	{
		return text;
	}
}
