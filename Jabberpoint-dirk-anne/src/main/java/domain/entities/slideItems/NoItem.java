package domain.entities.slideItems;

import domain.iterators.DefaultPresentationIterator;
import domain.iterators.PresentationElementIterator;
import domain.entities.*;
import domain.visitors.*;

public class NoItem extends SlideItem 
{
	@Override
	public void display(VisitorClient<PresentationElement> visitor) { }

	@Override
	public <TDTO> TDTO accept(MapPresentationElementToDTOVisitor<TDTO> visitor)
	{
		return visitor.visit(this);
	}

	@Override
	public PresentationElementIterator getIterator()
	{
		return new DefaultPresentationIterator(elements);
	}
}
