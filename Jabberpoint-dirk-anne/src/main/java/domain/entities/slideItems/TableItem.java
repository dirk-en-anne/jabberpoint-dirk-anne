package domain.entities.slideItems;

import java.util.List;
import domain.iterators.DefaultPresentationIterator;
import domain.iterators.PresentationElementIterator;
import domain.entities.*;
import domain.visitors.*;

public class TableItem extends SlideItem 
{
	private final List<TableRow> rows;
	
	public TableItem(List<TableRow> rows) 
	{
		this.rows = rows;
	}
	
	public List<TableRow> getRows() {
		return rows;
	}

	@Override
	public void display(VisitorClient<PresentationElement> visitor)
	{
		visitor.visit(this);
	}

	@Override
	public <TDTO> TDTO accept(MapPresentationElementToDTOVisitor<TDTO> visitor) 
	{
		return visitor.visit(this);
	}
	
	@Override
	public PresentationElementIterator getIterator()
	{
		return new DefaultPresentationIterator(elements);
	}
}
