package domain.entities.slideItems;

import domain.iterators.DefaultPresentationIterator;
import domain.iterators.PresentationElementIterator;
import domain.entities.*;
import domain.visitors.*;

public class MediaItem extends SlideItem 
{	
	private String source;
	
	public MediaItem(String source) 
	{
		this.source = source;
	}
	
	public String getSource() 
	{ 
		return source; 
	}

	@Override
	public void display(VisitorClient<PresentationElement> visitor)
	{
		visitor.visit(this);
	}

	@Override
	public <TDTO> TDTO accept(MapPresentationElementToDTOVisitor<TDTO> visitor)
	{
		return visitor.visit(this);
	}
	
	@Override
	public PresentationElementIterator getIterator()
	{
		return new DefaultPresentationIterator(elements);
	}
}
