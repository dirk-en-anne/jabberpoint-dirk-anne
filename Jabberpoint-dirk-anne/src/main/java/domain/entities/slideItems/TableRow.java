package domain.entities.slideItems;

import java.util.Iterator;
import java.util.List;

public class TableRow 
{
	private final List<String> columns;
	
	public TableRow(List<String> columns) 
	{ 
		this.columns = columns;
	}
	
	public List<String> getColumns() {
		return columns;
	}

	public Iterator<String> getIterator() 
	{
		return columns.iterator();
	}
}
