package domain.entities;

import java.util.ArrayList;

public abstract class SlideItem extends PresentationElement 
{
	protected SlideItem() 
	{
		super(new ArrayList<PresentationElement>(0));
	}
}
