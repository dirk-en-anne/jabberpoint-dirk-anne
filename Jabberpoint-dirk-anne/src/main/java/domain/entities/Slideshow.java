package domain.entities;

import java.util.List;

import domain.iterators.DefaultPresentationIterator;
import domain.iterators.PresentationElementIterator;
import domain.visitors.*;

public class Slideshow extends PresentationElement 
{
	private String title;
	
	public Slideshow(String title, List<PresentationElement> slides) 
	{
		super(slides);
		this.title = title;
	}
	
	public String getTitle() 
	{
		return title;
	}

	public void setTitle(String title) 
	{
		this.title = title;
	}
	
	@Override
	public void display(VisitorClient<PresentationElement> visitor)
	{
		visitor.visit(this);
	}

	@Override
	public <TDTO> TDTO accept(MapPresentationElementToDTOVisitor<TDTO> visitor) 
	{
		return visitor.visit(this);
	}
	
	@Override
	public PresentationElementIterator getIterator()
	{
		return new DefaultPresentationIterator(elements);
	}
	
	@Override
	public String toString() {
		return this.getTitle();
	}
}
