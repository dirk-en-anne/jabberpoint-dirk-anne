package domain.visitors;

public interface VisitorClient<TVisitable>
{
	void visit(TVisitable visitable);
}
