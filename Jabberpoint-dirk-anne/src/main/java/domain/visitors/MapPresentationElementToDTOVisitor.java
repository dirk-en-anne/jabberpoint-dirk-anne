package domain.visitors;

import domain.entities.*;
import domain.entities.slideItems.*;

public interface MapPresentationElementToDTOVisitor<TDTO> 
{
	TDTO visit(Slideshow slideshow);
	TDTO visit(Slide slide);
	TDTO visit(SlideComponent slideComponent);
	TDTO visit(MediaItem mediaItem);
	TDTO visit(NoItem noItem);
	TDTO visit(TableItem tableItem);
	TDTO visit(TextItem textItem);
	TDTO visit(TitleItem titleItem);
}