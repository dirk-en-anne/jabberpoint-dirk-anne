package domain.visitors;

import infrastructure.parsermodels.JabberpointRootElement;
import infrastructure.parsermodels.PresentationParserDTO;

public interface MapPresentationElementToParserDTOVisitor extends MapPresentationElementToDTOVisitor<PresentationParserDTO> 
{
	JabberpointRootElement getRootElement();
}
