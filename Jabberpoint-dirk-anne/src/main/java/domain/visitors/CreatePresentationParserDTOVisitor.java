package domain.visitors;

import java.util.ArrayList;

import domain.factories.PresentationParserDTOFactory;
import domain.entities.*;
import domain.entities.slideItems.*;
import infrastructure.builders.JabberpointRootElementBuilder;
import infrastructure.parsermodels.*;

public class CreatePresentationParserDTOVisitor implements MapPresentationElementToParserDTOVisitor {

	private final PresentationParserDTOFactory factory;
	private final JabberpointRootElementBuilder builder;
	
	public CreatePresentationParserDTOVisitor(PresentationParserDTOFactory factory, JabberpointRootElementBuilder builder) {
		this.factory = factory;
		this.builder = builder;
	}
	
	@Override
	public PresentationParserDTO visit(Slideshow slideshow) {
		var slideDTOs = new ArrayList<SlideParserDTO>();
		for(var slide : slideshow.getElements()) {
			slideDTOs.add((SlideParserDTO)slide.accept(this));
		}
		var slideshowDTO = factory.createSlideshow(slideshow, slideDTOs);
		builder.AddSlideshow((SlideshowParserDTO)slideshowDTO);
		return slideshowDTO;
	}

	@Override
	public PresentationParserDTO visit(Slide slide) {
		var componentDTOs = new ArrayList<SlideComponentParserDTO>();
		for(var component : slide.getElements()) {
			componentDTOs.add((SlideComponentParserDTO)component.accept(this));
		}
		var slideDTO = factory.createSlide(slide, componentDTOs);
		builder.AddSlide((SlideParserDTO)slideDTO);
		return slideDTO;
	}

	@Override
	public PresentationParserDTO visit(SlideComponent slideComponent) {
		var childrenDTOs = new ArrayList<PresentationParserDTO>();
		for(var component : slideComponent.getElements()) {
			childrenDTOs.add(component.accept(this));
		}
		var componentDTO = factory.createSlideComponent(slideComponent, childrenDTOs);
		builder.AddComponent((SlideComponentParserDTO)componentDTO);
		return componentDTO;
	}

	@Override
	public PresentationParserDTO visit(MediaItem mediaItem) {
		return factory.createMediaItem(mediaItem);
	}

	@Override
	public PresentationParserDTO visit(NoItem noItem) {
		return factory.createNoItem(noItem);
	}

	@Override
	public PresentationParserDTO visit(TableItem tableItem) {
		return factory.createTableItem(tableItem);
	}

	@Override
	public PresentationParserDTO visit(TextItem textItem) {
		return factory.createTextItem(textItem);
	}

	@Override
	public PresentationParserDTO visit(TitleItem titleItem) {
		return factory.createTitleItem(titleItem);
	}

	@Override
	public JabberpointRootElement getRootElement() {
		return builder.getRootElement();
	}

}
