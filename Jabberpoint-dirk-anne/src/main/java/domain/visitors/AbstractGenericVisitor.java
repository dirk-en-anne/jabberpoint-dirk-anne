package domain.visitors;

/**
 * This is the abstract class in between the Visitor<T> interface and the concrete visitor component.
 * This abstraction is used for implementing the structure of the decorator pattern by chaining multiple
 * concrete visitor components (each inheriting this abstraction) together into one visitor.
 */
public abstract class AbstractGenericVisitor<TBaseVisitable, TVisitable extends TBaseVisitable> implements GenericVisitor<TBaseVisitable>
{
	private final Class<TVisitable> type;
	private GenericVisitor<TBaseVisitable> next;
	
	/**
	 * The Java way of getting the type of the TVisitable generic. The concrete visitor component should provide the
	 * type it handles.
	 */
	protected AbstractGenericVisitor(Class<TVisitable> type)
	{
		this.type = type;
	}
	
	/**
	 * The template method that the concrete visitor component has to implement to provide
	 * the operation on the requested type.
	 */
	protected abstract void handle(TVisitable visitable);
	
	/**
	 * Generic implementation for visiting. Verifies whether the TBaseVisitable object is actually of type TVisitable.
	 * Otherwise another handler has to pick it up.
	 */
	public boolean visit(TBaseVisitable visitable)
	{
		if (visitable.getClass().isAssignableFrom(type))
		{
			handle(type.cast(visitable));
			return true;
		}
		
		return this.next != null 
			? this.next.visit(visitable)
			: false;
	}
	
	public void setNext(GenericVisitor<TBaseVisitable> next) 
	{
		if (this.next != null)
		{
			this.next.setNext(next);
			return;
		}
		
		this.next = next;
	}
}
