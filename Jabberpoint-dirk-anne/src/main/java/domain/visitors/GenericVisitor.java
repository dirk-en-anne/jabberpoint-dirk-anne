package domain.visitors;

public interface GenericVisitor<TVisitable> 
{
	boolean visit(TVisitable visitable);
	void setNext(GenericVisitor<TVisitable> next);
}
