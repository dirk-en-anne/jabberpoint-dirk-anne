package domain.iterators;

import java.util.List;

import domain.entities.PresentationElement;

public class DefaultPresentationIterator implements PresentationElementIterator 
{
	private List<PresentationElement> elements; 
	private int index;
	
	public DefaultPresentationIterator(List<PresentationElement> elements)
	{
		this.elements = elements;
		this.index = 0;
	}
	
	public boolean proceed() 
	{
		if (isEmpty() || (index + 1) >= elements.size())
		{
			return false;
		}
		
		index++;
		return true;
	}

	public boolean backtrack() 
	{
		if (isEmpty() || (index - 1) <= 0)
		{
			return false;
		}
		
		index--;
		return true;
	}

	public PresentationElement getCurrent() 
	{
		return isEmpty() ? null : elements.get(index);
	}

	public boolean isEmpty() 
	{
		return elements.isEmpty();
	}

	public boolean setCurrent(PresentationElement current) 
	{
		if (!elements.contains(current))
		{
			return false;
		}
		
		index = elements.indexOf(current);
		return true;
	}

	public boolean setCurrent(int index) 
	{
		if (index < 0 || index >= elements.size())
		{
			return false;
		}
		
		this.index = index;
		return true;
	}
}