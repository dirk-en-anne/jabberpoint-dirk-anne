package domain.iterators;

import domain.entities.PresentationElement;

public interface PresentationElementIterator 
{
	boolean proceed();
	boolean backtrack();
	PresentationElement getCurrent();
	boolean isEmpty();
	boolean setCurrent(PresentationElement current);
	boolean setCurrent(int index);
}
