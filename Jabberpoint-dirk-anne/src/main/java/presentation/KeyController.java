package presentation;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import application.PresentationServices;

public class KeyController extends KeyAdapter
{
	private PresentationServices presentation;
	
	public KeyController(PresentationServices presentation) 
	{
		this.presentation = presentation;
	}
	
	@Override
	public void keyPressed(KeyEvent keyEvent) 
	{
		switch (keyEvent.getKeyChar()) 
		{
			case KeyEvent.VK_UP:
			case KeyEvent.VK_RIGHT:
			case KeyEvent.VK_ENTER:
			case KeyEvent.VK_SPACE:
			case KeyEvent.VK_PAGE_UP:
			case '+':
				presentation.next();
				break;
			case KeyEvent.VK_DOWN:
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_BACK_SPACE:
			case KeyEvent.VK_PAGE_DOWN:
			case '-':
				presentation.previous();
				break;
			case KeyEvent.VK_ESCAPE:
			case KeyEvent.VK_DELETE:
				System.exit(0);
				break;
			case KeyEvent.VK_Q:
			case 'q':
				presentation.stop();
				break;
			default:
				break;
		}
	}
}
