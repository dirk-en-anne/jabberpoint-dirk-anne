package presentation;

import java.awt.*;
import java.awt.event.ActionListener;

import application.PresentationServices;
import application.factories.MenuBuilderFactory;
import presentation.userinterface.menus.*;

public class MenuController extends MenuBar
{
	private static final long serialVersionUID = 227L;
	
	public MenuController(
		PresentationServices presentation, 
		MenuBuilderFactory<Menu, MenuItem, ActionListener> builderFactory)
	{	
		this.add(FileMenu.generate(presentation, builderFactory.createBuilder()));
		this.add(ViewMenu.generate(presentation, builderFactory.createBuilder()));
		this.setHelpMenu(HelpMenu.generate(presentation, builderFactory.createBuilder()));
	}
}
