package presentation;

import java.util.function.Consumer;

import application.SlideshowProviderClient;
import domain.entities.*;
import infrastructure.*;
import presentation.userinterface.frames.SlideshowSelectionDialog;

public class DefaultPresentationProviderClient implements SlideshowProviderClient 
{
	private final PresentationRepository repository;
	private final PresentationParser parser;
	
	public DefaultPresentationProviderClient(PresentationRepository repository, PresentationParser parser) 
	{
		this.repository = repository;
		this.parser = parser;
	}
	
	public PresentationElement getSlideshowCollection() throws PresentationParserException
	{
		var stream = this.repository.get();
		if (stream == null)
		{
			System.out.println("No stream found.");
			return null;
		}
		
		System.out.println("Found a stream.");
		
		var slideshows = this.parser.parse(stream);
		if (slideshows.isEmpty())
		{
			System.out.println("Slideshows collection is empty.");
		}
		
		return new SlideshowCollection(slideshows);
	}
	
	public void selectPresentation(PresentationElement slideshows, Consumer<PresentationElement> listener)  
	{
		System.out.println("Opening the slideshow selection dialog.");
		SlideshowSelectionDialog.applyDialog(slideshows.getElements(), index -> 
		{
			var slideshow = slideshows.getElements().get(index);
			System.out.println("Selected slideshow: " + slideshow.toString());
			listener.accept(slideshow);
		});
	}
}
