package presentation.userinterface;

import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.awt.event.ActionListener;

import application.factories.builders.MenuItemBuilder;

public class AWTMenuItemBuilder implements MenuItemBuilder<MenuItem, ActionListener>
{
	private MenuItem menuItem;
	
	public AWTMenuItemBuilder() 
	{
		this.menuItem = new MenuItem();
	}
	
	@Override
	public MenuItemBuilder<MenuItem, ActionListener> withName(String name) 
	{
		menuItem.setName(name);
		menuItem.setLabel(name);
		menuItem.setShortcut(new MenuShortcut(name.charAt(0)));
		return this;
	}

	@Override
	public MenuItemBuilder<MenuItem, ActionListener> withListener(ActionListener listener) 
	{
		menuItem.addActionListener(listener);
		return this;
	}

	@Override
	public MenuItem build() 
	{
		return menuItem;
	}		
}
