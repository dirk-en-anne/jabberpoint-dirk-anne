package presentation.userinterface;

import java.io.*;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class FileDialog 
{	
	public static File open()
	{
		var fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Open presentation file");
		fileChooser.setFileFilter(new FileNameExtensionFilter("xml files (*.xml)", "xml"));
		fileChooser.setCurrentDirectory(new File("./../Sample"));
		
		var result = fileChooser.showOpenDialog(null);
		if (result == JFileChooser.APPROVE_OPTION)
		{
			var file = fileChooser.getSelectedFile();
			System.out.println("Opening: " + file.getName() + ".");
			return file;
		}
		
		System.out.println("Open command cancelled.");
		return null;
	}
	
	public static void save(byte[] data)
	{
		var fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Save presentation file");
		
		var result = fileChooser.showSaveDialog(null);
		if (result == JFileChooser.APPROVE_OPTION)
		{	
			saveFile(fileChooser.getSelectedFile(), data);
			return;
		}
		
		System.out.println("Save command cancelled.");
	}
	
	public static void saveFile(File file, byte[] data) 
	{
		OutputStream output = null;
		try {
			output = new FileOutputStream(file);
			output.write(data);
			output.flush();
			output.close();
			
			System.out.println("File " + file.getName() + " succesfully saved.");
		} catch (IOException exception) {
			exception.printStackTrace();
		} finally {
			try {
				output.close();
			} catch (IOException exception) {
				exception.printStackTrace();
			}
		}
	}
}
