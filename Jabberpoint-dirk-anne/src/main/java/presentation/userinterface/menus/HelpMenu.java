package presentation.userinterface.menus;

import java.awt.*;
import java.awt.event.ActionListener;

import application.PresentationServices;
import application.factories.builders.MenuBuilder;
import presentation.userinterface.menus.menuitems.help.AboutMenuItem;

public class HelpMenu extends AbstractMenu 
{
	protected HelpMenu(PresentationServices presentation, MenuBuilder<Menu, MenuItem, ActionListener> builder) 
	{
		super(presentation, builder);
	}

	@Override
	protected Menu create(PresentationServices presentation, MenuBuilder<Menu, MenuItem, ActionListener> builder) 
	{
		return builder
			.withName("Help")
			.attachMenuItem(menuItemBuilder -> AboutMenuItem.generate(presentation, menuItemBuilder))
			.build();
	}
	
	public static Menu generate(PresentationServices presentation, MenuBuilder<Menu, MenuItem, ActionListener> builder)
	{
		return new HelpMenu(presentation, builder).create();
	}
}
