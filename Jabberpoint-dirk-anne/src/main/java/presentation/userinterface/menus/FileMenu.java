package presentation.userinterface.menus;

import java.awt.*;
import java.awt.event.ActionListener;

import application.PresentationServices;
import application.factories.builders.MenuBuilder;
import presentation.userinterface.menus.menuitems.file.*;

public class FileMenu extends AbstractMenu 
{
	protected FileMenu(PresentationServices presentation, MenuBuilder<Menu, MenuItem, ActionListener> builder) 
	{
		super(presentation, builder);
	}

	@Override
	protected Menu create(PresentationServices presentation, MenuBuilder<Menu, MenuItem, ActionListener> builder) 
	{
		return builder
			.withName("File")
			.attachMenuItem(menuItemBuilder -> OpenMenuItem.generate(presentation, menuItemBuilder))
			.attachMenuItem(menuItemBuilder -> SaveMenuItem.generate(presentation, menuItemBuilder))
			.attachMenuItem(menuItemBuilder -> SaveAsMenuItem.generate(presentation, menuItemBuilder))
			.attachMenuItem(menuItemBuilder -> ExitMenuItem.generate(presentation, menuItemBuilder))
			.build();
	}
	
	public static Menu generate(PresentationServices presentation, MenuBuilder<Menu, MenuItem, ActionListener> builder)
	{
		return new FileMenu(presentation, builder).create();
	}
}
