package presentation.userinterface.menus;

import java.awt.*;
import java.awt.event.ActionListener;

import application.PresentationServices;
import application.factories.builders.MenuBuilder;

public abstract class AbstractMenu 
{
	private PresentationServices presentation;
	private MenuBuilder<Menu, MenuItem, ActionListener> builder;
	
	protected AbstractMenu(PresentationServices presentation, MenuBuilder<Menu, MenuItem, ActionListener> builder)
	{
		this.presentation = presentation;
		this.builder = builder;
	}
	
	protected abstract Menu create(PresentationServices presentation, MenuBuilder<Menu, MenuItem, ActionListener> builder);
	
	public Menu create() 
	{
		return create(presentation, builder);
	}
}
