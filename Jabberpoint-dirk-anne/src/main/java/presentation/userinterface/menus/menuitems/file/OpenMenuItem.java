package presentation.userinterface.menus.menuitems.file;

import java.awt.MenuItem;
import java.awt.event.ActionListener;

import application.PresentationServices;
import application.factories.builders.MenuItemBuilder;
import infrastructure.PresentationParserException;
import presentation.userinterface.menus.menuitems.AbstractMenuItem;

public class OpenMenuItem extends AbstractMenuItem 
{	
	protected OpenMenuItem(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder) 
	{
		super(presentation, builder);
	}

	@Override
	protected MenuItem create(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder) 
	{
		return builder
			.withName("Open")
			.withListener(event -> presentation.load())
			.build();
	}
	
	public static MenuItem generate(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder)
	{
		return new OpenMenuItem(presentation, builder).create();
	}
}
