package presentation.userinterface.menus.menuitems.help;

import java.awt.MenuItem;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import application.PresentationServices;
import application.factories.builders.MenuItemBuilder;
import jabberpointUI.jabberpoint;
import presentation.userinterface.menus.menuitems.AbstractMenuItem;

public class AboutMenuItem extends AbstractMenuItem 
{
	protected AboutMenuItem(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder) 
	{
		super(presentation, builder);
	}

	@Override
	protected MenuItem create(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder) 
	{
		return builder
			.withName("About")
			.withListener(event -> JOptionPane.showMessageDialog(jabberpoint.frame, "<about-text>"))
			.build();
	}
	
	public static MenuItem generate(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder)
	{
		return new AboutMenuItem(presentation, builder).create();
	}
}
