package presentation.userinterface.menus.menuitems.view;

import java.awt.MenuItem;
import java.awt.event.ActionListener;

import application.PresentationServices;
import application.factories.builders.MenuItemBuilder;
import presentation.userinterface.menus.menuitems.AbstractMenuItem;

public class NextMenuItem extends AbstractMenuItem 
{
	protected NextMenuItem(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder) 
	{
		super(presentation, builder);
	}

	@Override
	protected MenuItem create(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder) 
	{	
		return builder
			.withName("Next")
			.withListener(event -> presentation.next())
			.build();
	}
	
	public static MenuItem generate(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder)
	{
		return new NextMenuItem(presentation, builder).create();
	}
}
