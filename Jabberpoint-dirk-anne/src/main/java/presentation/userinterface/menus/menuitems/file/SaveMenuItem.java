package presentation.userinterface.menus.menuitems.file;

import java.awt.MenuItem;
import java.awt.event.ActionListener;

import application.PresentationServices;
import application.factories.builders.MenuItemBuilder;
import presentation.userinterface.menus.menuitems.AbstractMenuItem;

public class SaveMenuItem extends AbstractMenuItem 
{
	protected SaveMenuItem(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder) 
	{
		super(presentation, builder);
	}

	@Override
	protected MenuItem create(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder) 
	{
		return builder
			.withName("Save")
			.withListener(event -> 
			{
				
			})
			.build();
	}
	
	public static MenuItem generate(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder)
	{
		return new SaveMenuItem(presentation, builder).create();
	}
}
