package presentation.userinterface.menus.menuitems;

import java.awt.MenuItem;
import java.awt.event.ActionListener;

import application.PresentationServices;
import application.factories.builders.MenuItemBuilder;

public abstract class AbstractMenuItem 
{
	private PresentationServices presentation;
	private MenuItemBuilder<MenuItem, ActionListener> builder;
	
	protected AbstractMenuItem(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder)
	{
		this.presentation = presentation;
		this.builder = builder;
	}
	
	protected abstract MenuItem create(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder);
	
	public MenuItem create() 
	{
		return create(presentation, builder);
	}
}
