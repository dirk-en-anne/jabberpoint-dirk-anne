package presentation.userinterface.menus.menuitems.view;

import java.awt.MenuItem;
import java.awt.event.ActionListener;

import application.PresentationServices;
import application.factories.builders.MenuItemBuilder;
import presentation.userinterface.menus.menuitems.AbstractMenuItem;

public class PreviousMenuItem extends AbstractMenuItem 
{
	protected PreviousMenuItem(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder) 
	{
		super(presentation, builder);
	}

	@Override
	protected MenuItem create(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder) 
	{	
		return builder
			.withName("Previous")
			.withListener(event -> presentation.previous())
			.build();
	}
	
	public static MenuItem generate(PresentationServices presentation, MenuItemBuilder<MenuItem, ActionListener> builder)
	{
		return new PreviousMenuItem(presentation, builder).create();
	}
}
