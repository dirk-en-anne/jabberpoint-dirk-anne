package presentation.userinterface.menus;

import java.awt.*;
import java.awt.event.ActionListener;

import application.PresentationServices;
import application.factories.builders.MenuBuilder;
import presentation.userinterface.menus.menuitems.view.*;

public class ViewMenu extends AbstractMenu 
{
	protected ViewMenu(PresentationServices presentation, MenuBuilder<Menu, MenuItem, ActionListener> builder) 
	{
		super(presentation, builder);
	}

	@Override
	protected Menu create(PresentationServices presentation, MenuBuilder<Menu, MenuItem, ActionListener> builder) 
	{
		return builder
			.withName("View")
			.attachMenuItem(menuItemBuilder -> NextMenuItem.generate(presentation, menuItemBuilder))
			.attachMenuItem(menuItemBuilder -> PreviousMenuItem.generate(presentation, menuItemBuilder))
			.attachMenuItem(menuItemBuilder -> StartMenuItem.generate(presentation, menuItemBuilder))
			.attachMenuItem(menuItemBuilder -> StopMenuItem.generate(presentation, menuItemBuilder))
			.build();
	}
	
	public static Menu generate(PresentationServices presentation, MenuBuilder<Menu, MenuItem, ActionListener> builder)
	{
		return new ViewMenu(presentation, builder).create();
	}
}
