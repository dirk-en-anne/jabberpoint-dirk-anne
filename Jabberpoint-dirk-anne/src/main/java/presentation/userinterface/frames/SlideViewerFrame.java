package presentation.userinterface.frames;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;

import application.PresentationServices;
import application.factories.MenuBuilderFactory;
import presentation.*;
import presentation.userinterface.configuration.options.*;

public class SlideViewerFrame extends JFrame 
{
	private static final long serialVersionUID = 3227L;
	private static final String JabberPointTitle = "Jabberpoint Dirk Keizer & Anne Erdtsieck - 2020";
	
	private MenuBuilderFactory<Menu, MenuItem, ActionListener> builderFactory;
	private Options<DisplayOptionsValue> options;
	
	public SlideViewerFrame(MenuBuilderFactory<Menu, MenuItem, ActionListener> builderFactory, Options<DisplayOptionsValue> options) 
	{
		this.builderFactory = builderFactory;
		this.options = options;
		
		this.setBackground(Color.white);
	}
	
	public void setupWindow(JComponent component, PresentationServices presentation) 
	{
		this.setTitle(JabberPointTitle);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.addKeyListener(new KeyController(presentation));
		this.setMenuBar(new MenuController(presentation, builderFactory));
		this.getContentPane().add(component);
		
		var options = this.options.get();
		this.setSize(new Dimension(options.getSlideWidth(), options.getSlideHeight()));
		this.setBounds(100, 100, options.getSlideWidth(), options.getSlideHeight());
		this.setVisible(true);
	}
}
