package presentation.userinterface.frames;

import java.awt.BorderLayout;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.SwingConstants;

public class ListDialog<TItem>
{
 	private JList<TItem> list;
    private JLabel label;
    private JOptionPane optionPane;
    private JButton okButton;
    private JButton cancelButton;
    private ActionListener okEvent;
    private ActionListener cancelEvent;
    private JDialog dialog;

    public ListDialog(String message, JList<TItem> listToDisplay)
    {
        this.list = listToDisplay;
        this.label = new JLabel(message);
        createAndDisplayOptionPane();
    }

    public ListDialog(String title, String message, JList<TItem> listToDisplay)
    {
        this(message, listToDisplay);
        dialog.setTitle(title);
    }

    private void createAndDisplayOptionPane()
    {
        setupButtons();
        
        var pane = layoutComponents();
        optionPane = new JOptionPane(pane);
        optionPane.setOptions(new Object[]
        {
    		okButton, 
    		cancelButton
        });
        
        dialog = optionPane.createDialog("Select option");
    }

    private void setupButtons()
    {
        okButton = new JButton("Ok");
        okButton.addActionListener(e -> handleOkButtonClick(e));

        cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> handleCancelButtonClick(e));
    }

    private JPanel layoutComponents()
    {
        centerListElements();
        
        var panel = new JPanel(new BorderLayout(5,5));
        panel.add(label, BorderLayout.NORTH);
        panel.add(list, BorderLayout.CENTER);
        
        return panel;
    }

    private void centerListElements()
    {
        ((DefaultListCellRenderer)list.getCellRenderer()).setHorizontalAlignment(SwingConstants.CENTER);
    }

    public void setOnOk(ActionListener event)
    { 
    	okEvent = event; 
    }

    public void setOnClose(ActionListener event)
    {
        cancelEvent  = event;
    }

    private void handleOkButtonClick(ActionEvent e)
    {
        if (okEvent != null)
        {
        	okEvent.actionPerformed(e); 
        }
        
        hide();
    }

    private void handleCancelButtonClick(ActionEvent e)
    {
        if (cancelEvent != null)
        { 
        	cancelEvent.actionPerformed(e);
        }
        
        hide();
    }

    public void show()
    { 
    	dialog.setVisible(true); 
    
    }
    private void hide()
    { 
    	dialog.setVisible(false); 
    }
    
    public int getSelectedIndex() 
    {
    	return list.getSelectedIndex();
    }
}
