package presentation.userinterface.frames;

import java.util.List;
import java.util.function.Consumer;

import javax.swing.*;

import domain.entities.PresentationElement;

public class SlideshowSelectionDialog
{
	public static void applyDialog(List<PresentationElement> slideshows, Consumer<Integer> okListener)
	{
		var listModel = new DefaultListModel<String>();
		for (var slideshow : slideshows) 
		{
			listModel.addElement(slideshow.toString());
		}
		
		System.out.println("Found " + listModel.size() + " slideshows.");
		
		var slideshowSelections = new JList<String>(listModel);
		var dialog = new ListDialog<String>("Please select a slideshow: ", slideshowSelections);
		dialog.setOnOk(e -> okListener.accept(dialog.getSelectedIndex()));
		
		System.out.println("Setup of the dialog is completed, time to shine.");
		dialog.show();
	}
}
