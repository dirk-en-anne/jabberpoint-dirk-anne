package presentation.userinterface.frames;

import java.awt.*;

import javax.swing.JComponent;

import domain.entities.PresentationElement;
import presentation.userinterface.configuration.options.*;
import presentation.visitors.AbstractVisitorClient;

public class SlideViewerFrameComponent extends JComponent
{
	private static final long serialVersionUID = 227L;
	private Options<DisplayOptionsValue> options;
	private AbstractVisitorClient<PresentationElement> visitor;
	private PresentationElement slide;
	
	
	public SlideViewerFrameComponent(Options<DisplayOptionsValue> options, AbstractVisitorClient<PresentationElement> visitor)
	{
		super();
		
		this.options = options;
		this.visitor = visitor;
		
		this.setName(SlideViewerFrameComponent.class.getName());
		this.setOpaque(true);
		this.setBackground(Color.white);
	}
	
	public void update(PresentationElement slide) 
	{
		this.slide = slide;
		this.repaint();
	}
	
	@Override
	public void paintComponent(Graphics g) 
	{
		var options = this.options.get();
		var size = this.getPreferredSize();
		
		g.setColor(Color.white);
		if (slide != null)
		{
			g.setColor(Color.white);
			options.updateWidthOffset(0.f);
			options.updateHeightOffset(40);
			this.options.update(options);
		}
		
		g.fillRect(0, 0, size.width, size.height);
		g.setColor(Color.black);
		
		System.out.println("paint component");
		
		if (slide != null) 
		{
			visitor.setGraphics(g);
			slide.display(visitor);
		}
	}
	
	@Override
	public Dimension getPreferredSize() 
	{
		return new Dimension(1200, 800);
	}
}
