package presentation.userinterface;

import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.event.ActionListener;
import java.util.function.Function;

import application.factories.builders.MenuBuilder;
import application.factories.builders.MenuItemBuilder;

public class AWTMenuBuilder implements MenuBuilder<Menu, MenuItem, ActionListener>  
{
	private Menu menu;
	
	public AWTMenuBuilder() 
	{
		this.menu = new Menu();
	}
	
	public MenuBuilder<Menu, MenuItem, ActionListener> withName(String name) 
	{
		menu.setName(name);
		menu.setLabel(name);
		return this;
	}

	public MenuBuilder<Menu, MenuItem, ActionListener> attachMenuItem(Function<MenuItemBuilder<MenuItem, ActionListener>, MenuItem> builder) 
	{
		menu.add(builder.apply(new AWTMenuItemBuilder()));
		return this;
	}

	public Menu build() 
	{
		return menu;
	}
}
