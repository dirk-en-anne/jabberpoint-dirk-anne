package presentation.userinterface.algorithm.text;

import java.awt.Graphics;
import application.Style;

public interface TextDisplayAlgorithm 
{
	TextDisplayAlgorithm withScale(float scale);
	TextDisplayAlgorithm withGraphics(Graphics graphics);
	TextDisplayAlgorithm withStyle(Style style);
	TextDisplayAlgorithm withSlideWith(float slideWidth);
	void display(String text, int x, int y);
}
