package presentation.userinterface.algorithm.text;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.text.AttributedString;
import java.util.ArrayList;
import java.util.List;

public class DefaultTextDisplayAlgorithm extends AbstractTextDisplayAlgorithm 
{
	private List<TextLayout> getLayouts(String text, float width)
	{
		var attributedString = new AttributedString(text);
		attributedString.addAttribute(TextAttribute.FONT, style.getFont(scale), 0, text.length());
		
		var layouts = new ArrayList<TextLayout>();
		var graphics2D = (Graphics2D)graphics;
		var renderer = graphics2D.getFontRenderContext();
		var measurer = new LineBreakMeasurer(attributedString.getIterator(), renderer);
		
		float wrappingWidth = ((float)width - style.getIndent()) * scale;
		while (measurer.getPosition() < text.length())
		{
			layouts.add(measurer.nextLayout(wrappingWidth));
		}
		
		return layouts;
	}

	public void display(String text, int x, int y) 
	{
		var layouts = getLayouts(text, slideWidth);
		var pen = new Point(x + (int)(style.getIndent() * scale), y + (int)(style.getLeading() * scale));
		var graphics2D = (Graphics2D)graphics;
		
		graphics2D.setColor(style.getColor());
		
		var iterator = layouts.iterator();
		while (iterator.hasNext())
		{
			var layout = iterator.next();
			pen.y += layout.getAscent();
			layout.draw(graphics2D, pen.x, pen.y);
			pen.y += layout.getDescent();
		}
	}
}
