package presentation.userinterface.algorithm.text;

import java.awt.Graphics;

import application.Style;

public abstract class AbstractTextDisplayAlgorithm implements TextDisplayAlgorithm
{
	protected Graphics graphics;
	protected Style style;
	protected float scale;
	protected float slideWidth;
	
	public TextDisplayAlgorithm withScale(float scale) 
	{
		this.scale = scale;
		return this;
	}
	
	public TextDisplayAlgorithm withSlideWith(float slideWidth) 
	{
		this.slideWidth = slideWidth;
		return this;
	}

	public TextDisplayAlgorithm withGraphics(Graphics graphics) 
	{
		this.graphics = graphics;
		return this;
	}

	public TextDisplayAlgorithm withStyle(Style style) 
	{
		this.style = style;
		return this;
	}
}
