package presentation.userinterface.algorithm;

import java.awt.Graphics;
import java.awt.font.TextAttribute;
import java.text.AttributedString;

import application.StyleProvider;
import application.algorithms.DisplayAlgorithm;
import domain.entities.slideItems.TitleItem;
import presentation.userinterface.configuration.options.*;

public class TitleItemDisplayAlgorithm implements DisplayAlgorithm<TitleItem>
{
	private final StyleProvider styleProvider;
	private final Options<DisplayOptionsValue> options;
	
	public TitleItemDisplayAlgorithm(
		StyleProvider styleProvider,
		Options<DisplayOptionsValue> options) 
	{
		this.styleProvider = styleProvider;
		this.options = options;
	}
	
	public void display(Graphics graphics, TitleItem element) 
	{
		var style = this.styleProvider.getStyle(element.getLevel());
		var options = this.options.get();
		var text = element.getText();
		
		if (text == null || text.length() <= 0)
		{
			return;
		}
		
		var fontMetrics = graphics.getFontMetrics(style.getFont(options.getScale()));
		var attributedText = new AttributedString(text);
		attributedText.addAttribute(TextAttribute.FONT, fontMetrics.getFont(), 0, text.length());
		
		var x = options.getWidthOffset();
		graphics.drawString(attributedText.getIterator(), (int)x, (int)options.getHeightOffset());
		
		options.updateWidthOffset(x + (style.getIndent() * options.getScale()));
		options.updateHeightOffset(options.getHeightOffset() + fontMetrics.getHeight());
		this.options.update(options);
	}
}
