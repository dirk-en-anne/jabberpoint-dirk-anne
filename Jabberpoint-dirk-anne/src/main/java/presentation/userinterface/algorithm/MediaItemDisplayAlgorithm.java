package presentation.userinterface.algorithm;

import java.awt.Graphics;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import application.StyleProvider;
import application.algorithms.DisplayAlgorithm;
import domain.entities.slideItems.MediaItem;
import presentation.userinterface.configuration.options.*;

public class MediaItemDisplayAlgorithm implements DisplayAlgorithm<MediaItem>
{
	private final ImageObserver observer;
	private final StyleProvider styleProvider;
	private final Options<DisplayOptionsValue> options;
	
	public MediaItemDisplayAlgorithm(
		ImageObserver observer,
		StyleProvider styleProvider,
		Options<DisplayOptionsValue> options)
	{
		this.observer = observer;
		this.styleProvider = styleProvider;
		this.options = options;
	}
	
	public void display(Graphics graphics, MediaItem element) 
	{
		try 
		{
			var style = this.styleProvider.getStyle(element.getLevel());
			var options = this.options.get();
			var image = ImageIO.read(new File(element.getSource()));
			
			var x = (int)options.getWidthOffset() + (int)(style.getIndent() * options.getScale());
			var y = (int)options.getHeightOffset() + (int)(style.getIndent() * options.getScale());
			
			var width = (int)(image.getWidth(observer) * options.getScale());
			var height = (int)(image.getHeight(observer) * options.getScale());
			
			graphics.drawImage(image, x, y, width, height, observer);
			
			options.updateHeightOffset(y + height);
			this.options.update(options);
		}
		catch (IOException exception)
		{
			System.err.println("File at '" + element.getSource() + "' not found.");
		}
	}
}
