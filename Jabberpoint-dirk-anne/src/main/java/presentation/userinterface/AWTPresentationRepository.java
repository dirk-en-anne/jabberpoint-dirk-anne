package presentation.userinterface;

import java.io.*;
import infrastructure.PresentationRepository;

public class AWTPresentationRepository implements PresentationRepository
{
	@Override
	public InputStream get()
	{
		var file = FileDialog.open();
		if (file == null)
		{
			return null;
		}
		
		try {
			return new FileInputStream(file);
		} catch(Exception exception) {
			exception.printStackTrace();
			return null;
		}
	}

	@Override
	public void save(String filename, byte[] data)
	{
		if (filename == null || filename.isEmpty())
		{
			FileDialog.save(data);
			return;
		}
		
		FileDialog.saveFile(new File(filename), data);
	}
}
