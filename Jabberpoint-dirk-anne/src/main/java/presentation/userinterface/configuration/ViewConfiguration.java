package presentation.userinterface.configuration;

import java.awt.*;

public abstract class ViewConfiguration 
{
	protected Font font;
	protected Color backgroundColor;
	protected Color color;
	protected Dimension position;
	protected Dimension slideDimension;
	
	public Font getFont() { return font; }
	public Color getBackgroundColor() { return backgroundColor; }
	public Color getColor() { return color; }
	public Dimension getPosition() { return position; }
	public Dimension getSlideDimenion() { return slideDimension; }
}
