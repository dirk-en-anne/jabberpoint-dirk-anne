package presentation.userinterface.configuration;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

public class SlideViewConfiguration extends ViewConfiguration 
{
	public SlideViewConfiguration()
	{
		this.backgroundColor = Color.white;
		this.color = Color.black;
		this.font = new Font("Dialog", Font.BOLD, 10);
		this.position = new Dimension(1100, 20);
		this.slideDimension = new Dimension(); // TODO: Set slide dimension.
	}
}
