package presentation.userinterface.configuration;

import java.awt.Color;

import application.*;

public class DefaultStyleProvider implements StyleProvider
{
	private final Style[] styles;
	
	public DefaultStyleProvider() 
	{
		final String fontName = "Helvetica"; 
		styles = new Style[] 
		{
			new Style(0, Color.red, 20, fontName, 48),
			new Style(20, Color.blue, 10, fontName, 40),
			new Style(50, Color.black, 10, fontName, 36),
			new Style(70, Color.black, 10, fontName, 30),
			new Style(90, Color.black, 10, fontName, 24)
		};
	}
	
	@Override
	public Style getStyle(int level) 
	{
		if (level >= styles.length)
		{
			level = styles.length - 1;
		}
		
		return styles[level];
	}
}
