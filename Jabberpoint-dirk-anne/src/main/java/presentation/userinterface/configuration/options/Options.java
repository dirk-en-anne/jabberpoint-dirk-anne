package presentation.userinterface.configuration.options;

public interface Options<TValue> 
{
	TValue get();
	void update(TValue value);
}
