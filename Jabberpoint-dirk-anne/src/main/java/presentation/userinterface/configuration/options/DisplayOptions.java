package presentation.userinterface.configuration.options;

public class DisplayOptions implements Options<DisplayOptionsValue> 
{
	private DisplayOptionsValue optionsValue;
	
	public DisplayOptions(DisplayOptionsValue optionsValue) 
	{
		this.optionsValue = optionsValue;
	}

	public DisplayOptionsValue get() 
	{	
		return optionsValue;
	}

	public void update(DisplayOptionsValue value) 
	{
		this.optionsValue = value;
	}
}
