package presentation.userinterface.configuration.options;

public class DisplayOptionsValue
{
	private float scale;
	private float heightOffset;
	private float widthOffset;
	private int slideWidth;
	private int slideHeight;
	
	public DisplayOptionsValue(float scale, float widthOffset, float heightOffset, int slideWidth, int slideHeight) 
	{
		this.scale = scale;
		this.slideWidth = slideWidth;
		this.slideHeight = slideHeight;
		this.widthOffset = widthOffset;
		this.heightOffset = heightOffset;
	}
	
	public float getScale() { return scale; }
	public float getHeightOffset() { return heightOffset; }
	public float getWidthOffset() { return widthOffset; }
	
	public int getSlideWidth() { return slideWidth; }
	public int getSlideHeight() { return slideHeight; }
	
	public void updateScaleByComponentSize(float width, float height) 
	{
		this.scale = Math.min(width / slideWidth, height / slideHeight);
	}
	
	public void updateWidthOffset(float widthOffset)
	{
		this.widthOffset = widthOffset;
	}
	
	public void updateHeightOffset(float heightOffset) 
	{ 
		this.heightOffset = heightOffset;
	}
}
