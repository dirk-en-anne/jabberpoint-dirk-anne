package presentation.visitors;

import application.algorithms.DisplayAlgorithm;
import domain.entities.*;
import domain.visitors.*;

public final class DisplaySlideItemVisitor<TSlideItem extends SlideItem> extends AbstractGenericVisitor<PresentationElement, TSlideItem> 
{
	private final DisplayAlgorithm<TSlideItem> algorithm; 
	private AbstractVisitorClient<PresentationElement> client;
	
	public DisplaySlideItemVisitor(
		Class<TSlideItem> type, 
		AbstractVisitorClient<PresentationElement> client, 
		DisplayAlgorithm<TSlideItem> algorithm) 
	{
		super(type);
		
		this.algorithm = algorithm;
		this.client = client;
	}

	@Override
	protected void handle(TSlideItem visitable) 
	{
		algorithm.display(client.getGraphics(), visitable);
	}
}
