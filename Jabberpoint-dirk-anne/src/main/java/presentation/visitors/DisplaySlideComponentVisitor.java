package presentation.visitors;

import domain.visitors.*;
import domain.entities.*;

public final class DisplaySlideComponentVisitor extends AbstractGenericVisitor<PresentationElement, SlideComponent> 
{
	private AbstractVisitorClient<PresentationElement> client;
	
	public DisplaySlideComponentVisitor(AbstractVisitorClient<PresentationElement> client) 
	{
		super(SlideComponent.class);
		this.client = client;
	}

	@Override
	protected void handle(SlideComponent visitable) 
	{
		var iterator = visitable.getIterator();
		if (iterator.isEmpty())
		{
			return;
		}
		
		do 
		{
			iterator.getCurrent().display(this.client);
		}
		while (iterator.proceed());
	}
}
