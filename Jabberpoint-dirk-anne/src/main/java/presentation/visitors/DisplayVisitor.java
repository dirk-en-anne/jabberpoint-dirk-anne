package presentation.visitors;

import java.util.function.Function;

import domain.entities.PresentationElement;
import domain.visitors.*;

public class DisplayVisitor extends AbstractVisitorClient<PresentationElement> 
{
	public DisplayVisitor(Function<AbstractVisitorClient<PresentationElement>, GenericVisitor<PresentationElement>> builder) 
	{
		super(builder);
	}

}
