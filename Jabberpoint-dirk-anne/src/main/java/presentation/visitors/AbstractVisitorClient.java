package presentation.visitors;

import java.awt.Graphics;
import java.util.function.Function;
import domain.visitors.GenericVisitor;
import domain.visitors.VisitorClient;

public abstract class AbstractVisitorClient<TVisitable> implements VisitorClient<TVisitable> 
{
	private GenericVisitor<TVisitable> visitor;
	private Graphics graphics;
	
	protected AbstractVisitorClient(Function<AbstractVisitorClient<TVisitable>, GenericVisitor<TVisitable>> builder)
	{
		this.visitor = builder.apply(this);
	}
	
	public void setGraphics(Graphics graphics) 
	{
		this.graphics = graphics;
	}
	
	public Graphics getGraphics()
	{
		return this.graphics;
	}
	
	@Override
	public void visit(TVisitable visitable) 
	{
		this.visitor.visit(visitable);
	}
}
