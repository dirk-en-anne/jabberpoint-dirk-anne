package presentation.visitors;

import application.algorithms.DisplayAlgorithm;
import domain.entities.*;
import domain.visitors.*;
import jabberpointUI.jabberpoint;

public final class DisplaySlideVisitor extends AbstractGenericVisitor<PresentationElement, Slide> 
{
	private AbstractVisitorClient<PresentationElement> client;
	
	public DisplaySlideVisitor(AbstractVisitorClient<PresentationElement> client) 
	{
		super(Slide.class);
		this.client = client;
	}

	@Override
	protected void handle(Slide visitable) 
	{
		var iterator = visitable.getIterator();
		if (iterator.isEmpty())
		{
			return;
		}
		
		do 
		{
			iterator.getCurrent().display(this.client);
		}
		while(iterator.proceed());
	}
}
