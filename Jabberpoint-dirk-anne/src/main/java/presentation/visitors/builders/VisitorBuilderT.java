package presentation.visitors.builders;

import domain.visitors.GenericVisitor;

public interface VisitorBuilderT<TVisitable>
{
	VisitorBuilderT<TVisitable> attach(GenericVisitor<TVisitable> visitor);
	GenericVisitor<TVisitable> build();
}