package presentation.visitors.builders;

import domain.visitors.GenericVisitor;

/*
 * Constructs a decorative pipeline of visitor components.
 * In the end it will return a the root component.
 * */
public final class DefaultVisitorBuilderT<TVisitable> implements VisitorBuilderT<TVisitable> 
{
	private final GenericVisitor<TVisitable> rootVisitor;
	
	public DefaultVisitorBuilderT(GenericVisitor<TVisitable> rootVisitor)
	{
		this.rootVisitor = rootVisitor;
	}
	
	public VisitorBuilderT<TVisitable> attach(GenericVisitor<TVisitable> visitor) 
	{
		rootVisitor.setNext(visitor);
		return this;
	}

	public GenericVisitor<TVisitable> build() 
	{
		return rootVisitor;
	}
}
