package presentation.visitors.builders;

import domain.visitors.GenericVisitor;

/*
 * The default implementation for the builder that constructs decorative pipelines of visitor component.
 * It is a blank builder that can be asked to create a more in depth processing builder regarding a
 * TBaseVisitable type. 
 * */
public final class DefaultVisitorBuilder implements VisitorBuilder 
{
	public <TVisitable> VisitorBuilderT<TVisitable> attach(GenericVisitor<TVisitable> visitor) 
	{
		return new DefaultVisitorBuilderT<TVisitable>(visitor);
	}
	
	public static <TVisitable> VisitorBuilderT<TVisitable> startWith(GenericVisitor<TVisitable> visitor)
	{
		return new DefaultVisitorBuilder().attach(visitor);
	}
}
