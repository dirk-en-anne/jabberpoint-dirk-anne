package presentation.visitors.builders;

import domain.visitors.GenericVisitor;

public interface VisitorBuilder
{
	<TVisitable> VisitorBuilderT<TVisitable> attach(GenericVisitor<TVisitable> visitor);
}
