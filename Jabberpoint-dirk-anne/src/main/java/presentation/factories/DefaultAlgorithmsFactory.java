package presentation.factories;

import application.factories.AlgorithmsFactory;
import application.algorithms.*;
import domain.entities.Slide;
import domain.entities.slideItems.*;
import jabberpointUI.jabberpoint;
import presentation.userinterface.algorithm.*;

public class DefaultAlgorithmsFactory implements AlgorithmsFactory 
{	
	public DisplayAlgorithm<TextItem> createDisplayTextItemAlgorithm() 
	{
		return new TextItemDisplayAlgorithm(jabberpoint.styleProvider, jabberpoint.options);
	}

	public DisplayAlgorithm<TitleItem> createDisplayTitleItemAlgorithm() 
	{
		return new TitleItemDisplayAlgorithm(jabberpoint.styleProvider, jabberpoint.options);
	}

	public DisplayAlgorithm<MediaItem> createDisplayMediaItemAlgorithm() 
	{
		return new MediaItemDisplayAlgorithm(jabberpoint.component, jabberpoint.styleProvider, jabberpoint.options);
	}

	public DisplayAlgorithm<TableItem> createDisplayTableItemAlgorithm() 
	{
		// TODO Auto-generated method stub
		return null;
	}
}
