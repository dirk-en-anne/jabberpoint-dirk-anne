package presentation.factories;

import java.awt.MenuItem;
import java.awt.event.ActionListener;

import application.factories.MenuItemBuilderFactory;
import application.factories.builders.MenuItemBuilder;
import presentation.userinterface.AWTMenuItemBuilder;

public class AWTMenuItemBuilderFactory implements MenuItemBuilderFactory<MenuItem, ActionListener> 
{
	public MenuItemBuilder<MenuItem, ActionListener> createBuilder() 
	{
		return new AWTMenuItemBuilder();
	}
}
