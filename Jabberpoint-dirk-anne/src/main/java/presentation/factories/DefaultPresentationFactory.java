package presentation.factories;

import java.util.ArrayList;

import application.*;
import application.events.*;
import application.events.handlers.*;
import application.factories.*;
import domain.entities.PresentationElement;
import domain.events.*;
import domain.states.StateType;
import domain.visitors.VisitorClient;
import presentation.userinterface.configuration.DefaultStyleProvider;
import presentation.userinterface.configuration.options.*;
import presentation.userinterface.frames.JPanelThatShowsSlide;
import presentation.userinterface.frames.SlideViewerFrameComponent;

public class DefaultPresentationFactory implements PresentationFactory 
{
	private Configuration configuration;
	
	public DefaultPresentationFactory(Configuration configuration) 
	{
		this.configuration = configuration;
	}
	
	public PresentationServices CreatePresentationService(Options<DisplayOptionsValue> options) 
	{
		var provider = this.configuration.createStateProvider();
		var changedStateEventDispatcher = this.configuration.<ChangedStateEvent>createEventDispatcher();
		var displaySlideEventDispatcher = this.configuration.<DisplaySlideEvent>createEventDispatcher();
		
		var presentation = new Presentation(changedStateEventDispatcher, displaySlideEventDispatcher);
		
		var changedStateHandlers = new ArrayList<ChangedStateHandler>();
		changedStateHandlers.add(new LoadedStateHandler(presentation));
		changedStateHandlers.add(new DisplayingStateHandler(presentation));
		changedStateHandlers.add(new EndedStateHandler());
		
		var changedStateListener = new ChangedStateEventListener(changedStateHandlers);
		changedStateEventDispatcher.addListener(changedStateListener);
		
		var restrictedActionEventDispatcher = this.configuration.<RestrictedActionEvent>createEventDispatcher();
		restrictedActionEventDispatcher.addListener(new RestrictedActionEventListener());
		
		displaySlideEventDispatcher.addListener(new DisplaySlideEventListener(options));
		
		return presentation.setState(provider.createState(presentation, StateType.Unloaded, null, configuration.createClient(), restrictedActionEventDispatcher));
	}
}
