package presentation.factories;

import application.SlideshowProviderClient;
import application.events.DefaultEventDispatcher;
import application.factories.*;
import application.factories.state.*;
import domain.entities.PresentationElement;
import domain.events.*;
import domain.visitors.MapPresentationElementToParserDTOVisitor;
import domain.visitors.VisitorClient;
import infrastructure.*;
import infrastructure.parsermodels.PresentationParserDTO;
import presentation.*;
import presentation.userinterface.AWTPresentationRepository;
import presentation.visitors.DisplayVisitor;

public class DefaultConfiguration implements Configuration
{
	private VisitorFactory<PresentationElement, PresentationParserDTO> factory;
	
	public DefaultConfiguration(VisitorFactory<PresentationElement, PresentationParserDTO> factory)
	{
		this.factory = factory;
	}
	
	public PresentationRepository createRepository()
	{
		return new AWTPresentationRepository();
	}
	
	public PresentationStateProvider createStateProvider() 
	{
		return new DefaultPresentationStateProvider();
	}

	public PresentationParser createParser() 
	{
		return new DefaultPresentationParser(new XMLPresentationReader(), factory.CreateParserVisitor());
	}
	
	public SlideshowProviderClient createClient() 
	{
		return new DefaultPresentationProviderClient(createRepository(), createParser());
	}

	public PresentationWriter createWriter() 
	{
		return new XMLPresentationWriter((MapPresentationElementToParserDTOVisitor)factory.CreateWriterVisitor());
	}

	@Override
	public <TEvent> EventDispatcher<TEvent> createEventDispatcher() 
	{
		return new DefaultEventDispatcher<TEvent>();
	}

	@Override
	public VisitorClient<PresentationElement> createDisplayVisitor() 
	{
		return factory.createDisplayVisitor();
	}
}
