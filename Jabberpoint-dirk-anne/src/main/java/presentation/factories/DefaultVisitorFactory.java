package presentation.factories;

import application.factories.*;
import domain.entities.PresentationElement;
import domain.entities.slideItems.*;
import domain.factories.*;
import domain.repositories.InMemoryPresentationElementCache;
import domain.visitors.*;
import infrastructure.*;
import infrastructure.builders.DefaultJabberpointRootElementBuilder;
import infrastructure.parsermodels.PresentationParserDTO;
import presentation.visitors.*;
import presentation.visitors.builders.DefaultVisitorBuilder;

public class DefaultVisitorFactory implements VisitorFactory<PresentationElement, PresentationParserDTO>
{
	private AlgorithmsFactory factory;
	
	public DefaultVisitorFactory(AlgorithmsFactory factory)
	{
		this.factory = factory;
	}
	
	public VisitorClient<PresentationElement> createDisplayVisitor() 
	{
		return new DisplayVisitor(client -> 
		{
			var textAlgorithm = factory.createDisplayTextItemAlgorithm();
			var titleAlgorithm = factory.createDisplayTitleItemAlgorithm();
			var mediaAlgorithm = factory.createDisplayMediaItemAlgorithm();
			
			return DefaultVisitorBuilder
				.startWith(new DisplaySlideVisitor(client))
				.attach(new DisplaySlideComponentVisitor(client))
				.attach(new DisplaySlideItemVisitor<TextItem>(TextItem.class, client, textAlgorithm))
				.attach(new DisplaySlideItemVisitor<TitleItem>(TitleItem.class, client, titleAlgorithm))
				.attach(new DisplaySlideItemVisitor<MediaItem>(MediaItem.class, client, mediaAlgorithm))
				.build();
		});
	}

	public MapDTOToComponentVisitor<PresentationElement> CreateParserVisitor() 
	{
		return new CreatePresentationElementsVisitor(new InMemoryPresentationElementCache(), new DefaultPresentationElementFactory());
	}

	public MapPresentationElementToDTOVisitor<PresentationParserDTO> CreateWriterVisitor() 
	{
		return new CreatePresentationParserDTOVisitor(new DefaultPresentationParserDTOFactory(), new DefaultJabberpointRootElementBuilder());
	}
}
