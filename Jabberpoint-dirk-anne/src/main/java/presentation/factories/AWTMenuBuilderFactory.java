package presentation.factories;

import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.event.ActionListener;

import application.factories.MenuBuilderFactory;
import application.factories.builders.MenuBuilder;
import presentation.userinterface.AWTMenuBuilder;

public class AWTMenuBuilderFactory implements MenuBuilderFactory<Menu, MenuItem, ActionListener> 
{
	public MenuBuilder<Menu, MenuItem, ActionListener> createBuilder() 
	{
		return new AWTMenuBuilder();
	}
}
