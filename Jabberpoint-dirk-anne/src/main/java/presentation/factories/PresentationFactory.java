package presentation.factories;

import application.PresentationServices;
import domain.entities.PresentationElement;
import domain.visitors.VisitorClient;
import presentation.userinterface.configuration.options.*;

public interface PresentationFactory 
{
	PresentationServices CreatePresentationService(Options<DisplayOptionsValue> options);
}
