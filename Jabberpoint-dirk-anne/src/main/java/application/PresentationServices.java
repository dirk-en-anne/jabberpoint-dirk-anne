package application;

import domain.entities.PresentationElement;
import domain.events.DisplaySlideEvent;
import domain.events.EventDispatcher;
import domain.states.PresentationState;

public interface PresentationServices
{
	void load();
	void start();
	void unload();
	void next();
	void previous();
	void stop();
	PresentationServices setState(PresentationState state);
	EventDispatcher<DisplaySlideEvent> getDisplaySlideEventDispatcher();
}
