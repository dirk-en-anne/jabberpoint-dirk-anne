package application;

import java.util.function.Consumer;

import domain.entities.PresentationElement;
import infrastructure.PresentationParserException;

public interface SlideshowProviderClient 
{
	PresentationElement getSlideshowCollection() throws PresentationParserException;
	void selectPresentation(PresentationElement slideshows, Consumer<PresentationElement> listener);
}
