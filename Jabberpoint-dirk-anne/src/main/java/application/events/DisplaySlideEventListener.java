package application.events;

import domain.events.EventListener;
import jabberpointUI.jabberpoint;
import presentation.userinterface.configuration.options.*;
import domain.events.DisplaySlideEvent;

public class DisplaySlideEventListener implements EventListener<DisplaySlideEvent> 
{
	private final Options<DisplayOptionsValue> options;
	
	public DisplaySlideEventListener(Options<DisplayOptionsValue> options) 
	{
		this.options = options;
	}
	
	public void onEvent(DisplaySlideEvent event) 
	{
		jabberpoint.component.update(event.getSlide());
	}
}
