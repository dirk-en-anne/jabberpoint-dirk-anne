package application.events;

import domain.events.EventListener;

import java.util.List;

import application.events.handlers.ChangedStateHandler;
import domain.events.ChangedStateEvent;

public class ChangedStateEventListener implements EventListener<ChangedStateEvent> 
{
	private final List<ChangedStateHandler> handlers;
	
	public ChangedStateEventListener(List<ChangedStateHandler> handlers) 
	{
		this.handlers = handlers;
	}
	
	public void onEvent(ChangedStateEvent event) 
	{
		for (ChangedStateHandler changedStateHandler : handlers) 
		{
			if(changedStateHandler.canHandle(event.getStateType())) 
			{
				changedStateHandler.handle();
			}
		}	
	}
}
