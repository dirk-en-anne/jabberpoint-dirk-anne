package application.events;

import domain.events.EventListener;
import domain.events.RestrictedActionEvent;

public class RestrictedActionEventListener implements EventListener<RestrictedActionEvent> 
{
	public void onEvent(RestrictedActionEvent event) 
	{
		System.out.println("Encountered restricted action " + event.getName() + " because " + event.getReason() + ".");
	}	
}
