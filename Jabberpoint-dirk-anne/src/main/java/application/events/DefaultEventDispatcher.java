package application.events;

import domain.events.*;

public class DefaultEventDispatcher<TEvent> extends AbstractEventDispatcher<TEvent> 
{
	@Override
	protected void fireEvent(TEvent event) 
	{
		this.listeners.stream().forEach(listener -> listener.onEvent(event));
	}
}
