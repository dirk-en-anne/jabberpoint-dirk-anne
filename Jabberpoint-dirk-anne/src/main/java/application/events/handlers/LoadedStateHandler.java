package application.events.handlers;

import application.PresentationServices;
import domain.states.StateType;

public class LoadedStateHandler implements ChangedStateHandler 
{
	private final PresentationServices presentationServices;
	
	public LoadedStateHandler(PresentationServices presentationServices) 
	{
		this.presentationServices = presentationServices;
	}
	
	@Override
	public boolean canHandle(StateType type) 
	{
		return type == StateType.Loaded;
	}

	@Override
	public void handle() 
	{
		presentationServices.load();
	}

}
