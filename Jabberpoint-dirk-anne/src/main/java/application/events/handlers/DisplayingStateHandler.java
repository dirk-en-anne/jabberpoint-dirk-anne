package application.events.handlers;

import application.PresentationServices;
import domain.states.StateType;

public class DisplayingStateHandler implements ChangedStateHandler 
{
	private final PresentationServices presentationServices;
	
	public DisplayingStateHandler(PresentationServices presentationServices) 
	{
		this.presentationServices = presentationServices;
	}
	
	@Override
	public boolean canHandle(StateType type) 
	{
		return type == StateType.Displaying;
	}

	@Override
	public void handle() 
	{
		presentationServices.start();
	}
}
