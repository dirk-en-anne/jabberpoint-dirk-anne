package application.events.handlers;

import domain.states.StateType;

public interface ChangedStateHandler 
{
	boolean canHandle(StateType type);
	void handle();
}
