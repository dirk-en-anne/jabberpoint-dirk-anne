package application.events.handlers;

import domain.entities.PresentationElement;
import domain.states.StateType;
import jabberpointUI.jabberpoint;

public class EndedStateHandler implements ChangedStateHandler
{
	@Override
	public boolean canHandle(StateType type) 
	{
		return type == StateType.Ended;
	}

	@Override
	public void handle() 
	{
		PresentationElement element = null;
		jabberpoint.component.update(element);
	}
}
