package application.state;

import application.PresentationServices;
import application.SlideshowProviderClient;
import application.factories.state.*;
import domain.entities.PresentationElement;
import domain.events.EventDispatcher;
import domain.events.RestrictedActionEvent;
import domain.iterators.PresentationElementIterator;
import domain.states.StateType;

public class DisplayingPresentationState extends AbstractPresentationState 
{
	private PresentationElement currentSlide;
	private SlideshowProviderClient client;
	
	public DisplayingPresentationState(
		PresentationServices context, 
		PresentationStateProvider provider, 
		PresentationElement slideshow,
		SlideshowProviderClient client,
		EventDispatcher<RestrictedActionEvent> dispatcher) 
	{
		super(context, provider, slideshow, dispatcher, DisplayingPresentationState.class.getName());
		this.client = client;
	}

	public void unload() { this.Restricted("unload"); }
	public void load() { this.Restricted("load"); }

	public void start() 
	{
		var iterator = this.slideshow.getIterator();
		this.setupCurrentSituation(iterator);
		this.DisplayCurrentSlide(currentSlide);
	}
	
	public void next() 
	{
		var iterator = this.slideshow.getIterator();
		this.setupCurrentSituation(iterator);
		if (!iterator.proceed())
		{
			stop();
		}
		
		currentSlide = iterator.getCurrent();
		this.DisplayCurrentSlide(currentSlide);
	}

	public void previous() 
	{
		var iterator = this.slideshow.getIterator();
		this.setupCurrentSituation(iterator);
		if (!iterator.backtrack())
		{
			return;
		}
		
		currentSlide = iterator.getCurrent();
		this.DisplayCurrentSlide(currentSlide);
	}
	
	public void stop() 
	{ 
		this.context.setState(this.provider.createState(context, StateType.Ended, this.slideshow, this.client, this.dispatcher));
	}
	
	private void setupCurrentSituation(PresentationElementIterator iterator)
	{
		if (currentSlide == null)
		{
			currentSlide = iterator.getCurrent();
		}
		else 
		{
			iterator.setCurrent(currentSlide);
		}
	}
	
	public StateType getType() 
	{
		return StateType.Displaying;
	}
}
