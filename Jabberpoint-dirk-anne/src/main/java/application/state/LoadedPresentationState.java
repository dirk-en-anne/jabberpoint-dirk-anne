package application.state;

import application.PresentationServices;
import application.SlideshowProviderClient;
import application.factories.state.*;
import domain.entities.PresentationElement;
import domain.entities.SlideshowCollection;
import domain.events.EventDispatcher;
import domain.events.RestrictedActionEvent;
import domain.states.StateType;

public class LoadedPresentationState extends AbstractPresentationState 
{
	private SlideshowProviderClient client;
	
	public LoadedPresentationState(
		PresentationServices context, 
		PresentationStateProvider provider, 
		PresentationElement slideshow,
		SlideshowProviderClient client,
		EventDispatcher<RestrictedActionEvent> dispatcher) 
	{
		super(context, provider, slideshow, dispatcher, LoadedPresentationState.class.getName());
		this.client = client;
	}

	public void start() 
	{
		this.context.setState(this.provider.createState(context, StateType.Displaying, slideshow, this.client, this.dispatcher));
	}

	public void load() 
	{ 
		if (this.slideshow instanceof SlideshowCollection)
		{
			this.client.selectPresentation(slideshow, element -> slideshow = element);
			start();
		}
	}
	
	public void next() { this.Restricted("next"); }
	public void previous() { this.Restricted("previous"); }
	public void stop() { this.Restricted("stop"); }

	public void unload() 
	{
		this.context.setState(this.provider.createState(context, StateType.Unloaded, null, this.client, this.dispatcher));
	}

	public StateType getType() 
	{
		return StateType.Loaded;
	}
}
