package application.state;

import application.PresentationServices;
import application.factories.state.*;
import domain.entities.PresentationElement;
import domain.events.*;
import domain.states.PresentationState;

public abstract class AbstractPresentationState implements PresentationState 
{
	protected PresentationServices context;
	protected PresentationStateProvider provider;
	protected PresentationElement slideshow;
	protected EventDispatcher<RestrictedActionEvent> dispatcher;
	private String reason;
	
	protected AbstractPresentationState(
		PresentationServices context, 
		PresentationStateProvider provider, 
		PresentationElement slideshow,
		EventDispatcher<RestrictedActionEvent> dispatcher,
		String name)
	{
		this.context = context;
		this.provider = provider;
		this.slideshow = slideshow;
		this.dispatcher = dispatcher;
		this.reason = name + " does not support this action";
	}
	
	protected void Restricted(String name) { Restricted(name, reason); }
	protected void Restricted(String name, String reason)
	{
		dispatcher.createEvent(new RestrictedActionEvent(name, reason));
	}
	
	protected void DisplayCurrentSlide(PresentationElement currentSlide)
	{
		context
			.getDisplaySlideEventDispatcher()
			.createEvent(new DisplaySlideEvent(currentSlide));
	}
}
