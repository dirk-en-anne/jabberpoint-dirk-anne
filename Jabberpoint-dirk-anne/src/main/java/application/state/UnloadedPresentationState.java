package application.state;

import application.PresentationServices;
import application.SlideshowProviderClient;
import application.factories.state.*;
import domain.entities.PresentationElement;
import domain.events.EventDispatcher;
import domain.events.RestrictedActionEvent;
import domain.states.StateType;
import infrastructure.PresentationParserException;

public class UnloadedPresentationState extends AbstractPresentationState 
{
	private SlideshowProviderClient client;
	
	public UnloadedPresentationState(
		PresentationServices context, 
		PresentationStateProvider provider, 
		PresentationElement slideshow,
		SlideshowProviderClient client,
		EventDispatcher<RestrictedActionEvent> dispatcher) 
	{
		super(context, provider, slideshow, dispatcher, UnloadedPresentationState.class.getName());
		this.client = client;
	}

	public void load() 
	{
		try 
		{
			var slideshows = this.client.getSlideshowCollection();
			this.context.setState(this.provider.createState(context, StateType.Loaded, slideshows, client, this.dispatcher));
		} 
		catch(PresentationParserException exception) 
		{
			exception.printStackTrace();
		}
	}

	public void start() { this.Restricted("start"); }
	public void unload() { this.Restricted("unload"); }
	public void next() { this.Restricted("next"); }
	public void previous() { this.Restricted("previous"); }
	public void stop() { this.Restricted("stop"); }
	
	public StateType getType() 
	{
		return StateType.Unloaded;
	}
}
