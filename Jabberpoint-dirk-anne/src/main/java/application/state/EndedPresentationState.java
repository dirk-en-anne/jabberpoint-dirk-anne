package application.state;

import application.PresentationServices;
import application.SlideshowProviderClient;
import application.factories.state.*;
import domain.entities.PresentationElement;
import domain.events.EventDispatcher;
import domain.events.RestrictedActionEvent;
import domain.states.StateType;

public class EndedPresentationState extends AbstractPresentationState 
{
	private SlideshowProviderClient client;
	
	public EndedPresentationState(
		PresentationServices context, 
		PresentationStateProvider provider, 
		PresentationElement slideshow, 
		SlideshowProviderClient client,
		EventDispatcher<RestrictedActionEvent> dispatcher) 
	{
		super(context, provider, slideshow, dispatcher, EndedPresentationState.class.getName());
		this.client = client;
	}

	public void start() 
	{
		this.context.setState(this.provider.createState(context, StateType.Displaying, slideshow, this.client, this.dispatcher));
	}

	public void load() { this.Restricted("load"); }
	public void next() { unload(); }
	public void previous() { this.Restricted("previous"); }
	public void stop() { unload(); }

	public void unload() 
	{
		this.context.setState(this.provider.createState(context, StateType.Unloaded, null, this.client, this.dispatcher));
	}

	public StateType getType() 
	{
		return StateType.Ended;
	}
}
