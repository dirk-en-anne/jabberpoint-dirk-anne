package application;

public interface StyleProvider
{
	Style getStyle(int level);
}
