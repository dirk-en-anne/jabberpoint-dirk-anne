package application.algorithms;

import java.awt.Graphics;

import domain.entities.PresentationElement;

public interface DisplayAlgorithm<TPresentationElement extends PresentationElement>
{
	void display(Graphics graphics, TPresentationElement element);
}
