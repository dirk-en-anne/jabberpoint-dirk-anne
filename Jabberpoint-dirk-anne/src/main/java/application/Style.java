package application;

import java.awt.Color;
import java.awt.Font;

public class Style 
{
	private int indent;
	private int fontSize;
	private Color color;
	private String fontName;
	private float leading;
	
	public Style(int indent, Color color, int leading, String fontName, int fontSize) 
	{
		this.indent = indent;
		this.color = color;
		this.fontName = fontName;
		this.fontSize = fontSize;
		this.leading = leading;
	}
	
	public int getIndent() { return indent; }
	public Color getColor() { return color; }
	public float getLeading() { return leading; }
	
	public Font getFont(float scale)
	{
		return new Font(this.fontName, Font.BOLD, this.fontSize).deriveFont(this.fontSize * scale);
	}
}
