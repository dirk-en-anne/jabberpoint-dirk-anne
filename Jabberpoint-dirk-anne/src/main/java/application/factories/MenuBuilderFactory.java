package application.factories;

import java.util.EventListener;
import application.factories.builders.MenuBuilder;

public interface MenuBuilderFactory<TMenu, TMenuItem, TEventListener extends EventListener> 
{
	MenuBuilder<TMenu, TMenuItem, TEventListener> createBuilder();
}
