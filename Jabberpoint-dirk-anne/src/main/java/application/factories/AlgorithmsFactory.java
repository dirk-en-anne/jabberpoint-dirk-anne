package application.factories;

import application.algorithms.*;
import domain.entities.Slide;
import domain.entities.slideItems.*;

public interface AlgorithmsFactory 
{
	DisplayAlgorithm<TextItem> createDisplayTextItemAlgorithm();
	DisplayAlgorithm<TitleItem> createDisplayTitleItemAlgorithm();
	DisplayAlgorithm<MediaItem> createDisplayMediaItemAlgorithm();
	DisplayAlgorithm<TableItem> createDisplayTableItemAlgorithm();
}
