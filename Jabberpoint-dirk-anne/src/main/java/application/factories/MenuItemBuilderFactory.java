package application.factories;

import java.util.EventListener;
import application.factories.builders.MenuItemBuilder;

public interface MenuItemBuilderFactory<TMenuItem, TEventListener extends EventListener> 
{
	MenuItemBuilder<TMenuItem, TEventListener> createBuilder();
}
