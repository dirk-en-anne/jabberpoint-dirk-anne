package application.factories.builders;

import java.util.EventListener;

public interface MenuItemBuilder<TMenuItem, TEventListener extends EventListener>
{
	MenuItemBuilder<TMenuItem, TEventListener> withName(String name);
	MenuItemBuilder<TMenuItem, TEventListener> withListener(TEventListener listener);
	TMenuItem build();
}
