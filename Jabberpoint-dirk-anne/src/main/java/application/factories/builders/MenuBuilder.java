package application.factories.builders;

import java.util.EventListener;
import java.util.function.Function;

public interface MenuBuilder<TMenu, TMenuItem, TEventListener extends EventListener> 
{
	MenuBuilder<TMenu, TMenuItem, TEventListener> withName(String name);
	MenuBuilder<TMenu, TMenuItem, TEventListener> attachMenuItem(Function<MenuItemBuilder<TMenuItem, TEventListener>, TMenuItem> builder);
	TMenu build();
}
