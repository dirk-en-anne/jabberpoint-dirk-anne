package application.factories.state;

import application.PresentationServices;
import application.SlideshowProviderClient;
import domain.entities.PresentationElement;
import domain.events.EventDispatcher;
import domain.events.RestrictedActionEvent;
import domain.states.PresentationState;
import domain.states.StateType;

public interface PresentationStateProvider 
{
	PresentationState createState(
		PresentationServices context, 
		StateType stateType, 
		PresentationElement element, 
		SlideshowProviderClient client, 
		EventDispatcher<RestrictedActionEvent> dispatcher);
}
