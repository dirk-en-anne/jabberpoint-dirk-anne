package application.factories.state;

import java.util.List;

import application.PresentationServices;
import application.SlideshowProviderClient;
import domain.entities.PresentationElement;
import domain.events.EventDispatcher;
import domain.events.RestrictedActionEvent;
import domain.states.PresentationState;
import domain.states.StateType;

/**
 * The abstraction for the state provider. This provider holds a collection of state factories where 
 * from the requested one can be chosen by querying on the state type. There after it is used to 
 * create the corresponding state object.
 */
public abstract class AbstractPresentationStateProvider implements PresentationStateProvider 
{
	protected List<PresentationStateFactory> factories;
	
	@Override
	public PresentationState createState(
		PresentationServices context, 
		StateType stateType, 
		PresentationElement element,
		SlideshowProviderClient client,
		EventDispatcher<RestrictedActionEvent> dispatcher) 
	{
		for (var factory : factories)
		{
			if (factory.isSatisfiedBy(stateType))
			{
				return factory.CreateState(context, element, client, dispatcher);
			}
		}
		
		return null;
	}
}
