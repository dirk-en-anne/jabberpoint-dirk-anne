package application.factories.state;

import application.PresentationServices;
import application.SlideshowProviderClient;
import application.state.*;
import domain.entities.PresentationElement;
import domain.events.EventDispatcher;
import domain.events.RestrictedActionEvent;
import domain.states.PresentationState;
import domain.states.StateType;

public class UnloadedPresentationStateFactory implements PresentationStateFactory
{
	private PresentationStateProvider provider;
	
	public UnloadedPresentationStateFactory(PresentationStateProvider provider)
	{
		this.provider = provider;
	}
	
	public boolean isSatisfiedBy(StateType stateType) 
	{
		return stateType == StateType.Unloaded;
	}

	public PresentationState CreateState(
		PresentationServices context, 
		PresentationElement element,
		SlideshowProviderClient client,
		EventDispatcher<RestrictedActionEvent> dispatcher) 
	{
		return new UnloadedPresentationState(context, provider, element, client, dispatcher);
	}	
}
