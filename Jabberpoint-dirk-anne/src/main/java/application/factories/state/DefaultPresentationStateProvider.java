package application.factories.state;

import java.util.ArrayList;
import java.util.List;

import application.PresentationServices;
import domain.entities.PresentationElement;
import domain.states.PresentationState;
import domain.states.StateType;

public class DefaultPresentationStateProvider extends AbstractPresentationStateProvider 
{
	public DefaultPresentationStateProvider()
	{
		this.factories = getFactories();
	}
	
	private List<PresentationStateFactory> getFactories() 
	{
		var factoryCollection = new ArrayList<PresentationStateFactory>();
		
		factoryCollection.add(new LoadedPresentationStateFactory(this));
		factoryCollection.add(new UnloadedPresentationStateFactory(this));
		factoryCollection.add(new DisplayingPresentationStateFactory(this));
		factoryCollection.add(new EndedPresentationStateFactory(this));
		
		return factoryCollection;
	}
}
