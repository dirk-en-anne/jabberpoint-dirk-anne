package application.factories.state;

import application.PresentationServices;
import application.SlideshowProviderClient;
import application.state.LoadedPresentationState;
import domain.entities.PresentationElement;
import domain.events.EventDispatcher;
import domain.events.RestrictedActionEvent;
import domain.states.PresentationState;
import domain.states.StateType;

public class LoadedPresentationStateFactory implements PresentationStateFactory
{
	private PresentationStateProvider provider;
	
	public LoadedPresentationStateFactory(PresentationStateProvider provider)
	{
		this.provider = provider;
	}
	
	public boolean isSatisfiedBy(StateType stateType) 
	{
		return stateType == StateType.Loaded;
	}

	public PresentationState CreateState(
		PresentationServices context, 
		PresentationElement presentationElement, 
		SlideshowProviderClient client,
		EventDispatcher<RestrictedActionEvent> dispatcher) 
	{
		return new LoadedPresentationState(context, provider, presentationElement, client, dispatcher);
	}	
}
