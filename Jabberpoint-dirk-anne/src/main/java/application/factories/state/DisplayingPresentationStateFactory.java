package application.factories.state;

import application.PresentationServices;
import application.SlideshowProviderClient;
import application.state.*;
import domain.entities.PresentationElement;
import domain.events.EventDispatcher;
import domain.events.RestrictedActionEvent;
import domain.states.PresentationState;
import domain.states.StateType;

public class DisplayingPresentationStateFactory implements PresentationStateFactory
{
	private PresentationStateProvider provider;
	
	public DisplayingPresentationStateFactory(PresentationStateProvider provider)
	{
		this.provider = provider;
	}
	
	public boolean isSatisfiedBy(StateType stateType) 
	{
		return stateType == StateType.Displaying;
	}

	public PresentationState CreateState(
		PresentationServices context, 
		PresentationElement presentationElement,
		SlideshowProviderClient client,
		EventDispatcher<RestrictedActionEvent> dispatcher) 
	{
		return new DisplayingPresentationState(context, provider, presentationElement, client, dispatcher);
	}
}
