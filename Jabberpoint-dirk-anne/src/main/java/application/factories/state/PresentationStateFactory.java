package application.factories.state;

import application.PresentationServices;
import application.SlideshowProviderClient;
import domain.entities.PresentationElement;
import domain.events.EventDispatcher;
import domain.events.RestrictedActionEvent;
import domain.states.PresentationState;
import domain.states.StateType;

/*
 * The interface for the state factories. It uses the specification pattern 
 * structure for the means of querying for the right factory in the provider.
 * */
public interface PresentationStateFactory 
{
	boolean isSatisfiedBy(StateType stateType);
	
	PresentationState CreateState(
		PresentationServices context, 
		PresentationElement element, 
		SlideshowProviderClient client,
		EventDispatcher<RestrictedActionEvent> dispatcher);
}
