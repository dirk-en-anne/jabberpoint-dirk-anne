package application.factories;

import domain.entities.PresentationElement;
import domain.visitors.*;
import infrastructure.MapDTOToComponentVisitor;

public interface VisitorFactory<TComponent, TDTO> 
{
	VisitorClient<PresentationElement> createDisplayVisitor();
	MapDTOToComponentVisitor<TComponent> CreateParserVisitor();
	MapPresentationElementToDTOVisitor<TDTO> CreateWriterVisitor();
}
