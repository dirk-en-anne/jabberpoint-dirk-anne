package application.factories;

import application.SlideshowProviderClient;
import application.factories.state.PresentationStateProvider;
import domain.entities.PresentationElement;
import domain.events.*;
import domain.visitors.VisitorClient;
import infrastructure.*;

public interface Configuration
{
	PresentationRepository createRepository();
	PresentationStateProvider createStateProvider();
	PresentationParser createParser();
	PresentationWriter createWriter();
	SlideshowProviderClient createClient();
	<TEvent> EventDispatcher<TEvent> createEventDispatcher();
	VisitorClient<PresentationElement> createDisplayVisitor();
}
