package application;

import domain.states.PresentationState;
import domain.events.*;

public class Presentation implements PresentationServices 
{
	private EventDispatcher<ChangedStateEvent> changedStateEventDispatcher;
	private EventDispatcher<DisplaySlideEvent> displaySlideEventDispatcher;
	
	private PresentationState state;
	
	public Presentation(
		EventDispatcher<ChangedStateEvent> changedStateEventDispatcher, 
		EventDispatcher<DisplaySlideEvent> displaySlideEventDispatcher)
	{
		this.changedStateEventDispatcher = changedStateEventDispatcher;
		this.displaySlideEventDispatcher = displaySlideEventDispatcher;
	}
	
	public void load() 
	{
		if (this.state != null)
		{
			this.state.load();
		}
	}

	public void start()
	{
		if (this.state != null)
		{
			this.state.start();
		}
	}

	public void unload() 
	{
		if (this.state != null)
		{
			this.state.unload();
		}
	}

	public void next() 
	{
		if (this.state != null)
		{
			this.state.next();
		}
	}

	public void previous() 
	{
		if (this.state != null)
		{
			this.state.previous();
		}
	}

	public void stop() 
	{
		if (this.state != null)
		{
			this.state.stop();
		}
	}

	public PresentationServices setState(PresentationState state) 
	{
		this.state = state;
		this.changedStateEventDispatcher.createEvent(new ChangedStateEvent(state.getType()));
		return this;
	}
	
	public EventDispatcher<DisplaySlideEvent> getDisplaySlideEventDispatcher()
	{
		return this.displaySlideEventDispatcher;
	}
}
