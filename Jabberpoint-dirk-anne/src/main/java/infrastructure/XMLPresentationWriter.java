package infrastructure;

import java.io.OutputStream;
import java.util.List;
import javax.xml.bind.JAXBException;

import domain.entities.PresentationElement;
import domain.visitors.MapPresentationElementToParserDTOVisitor;

public class XMLPresentationWriter implements PresentationWriter 
{
	private final MapPresentationElementToParserDTOVisitor visitor;
	
	public XMLPresentationWriter(MapPresentationElementToParserDTOVisitor visitor) 
	{
		this.visitor = visitor;
	}
	
	public void Write(OutputStream outputStream, List<PresentationElement> presentationElements) throws JAXBException 
	{
		for(var slideshow : presentationElements)
		{
			slideshow.accept(visitor);
		}
		var rootElement = visitor.getRootElement();
		
		javax.xml.bind.JAXBContext jaxbCtx = javax.xml.bind.JAXBContext.newInstance(rootElement.getClass().getPackage().getName());
        javax.xml.bind.Marshaller marshaller = jaxbCtx.createMarshaller();
        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(rootElement, System.out);
        marshaller.marshal(rootElement, outputStream);
	}
}
