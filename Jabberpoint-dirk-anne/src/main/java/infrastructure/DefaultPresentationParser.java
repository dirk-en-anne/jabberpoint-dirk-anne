package infrastructure;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import domain.entities.PresentationElement;

public class DefaultPresentationParser implements PresentationParser 
{	
	private final PresentationReader presentationReader;
	private final MapDTOToComponentVisitor<PresentationElement> mapDTOToPresentationElementsVisitor;

    public DefaultPresentationParser(PresentationReader presentationReader, MapDTOToComponentVisitor<PresentationElement> mapDTOToPresentationElementsVisitor) {
        this.presentationReader = presentationReader;
        this.mapDTOToPresentationElementsVisitor = mapDTOToPresentationElementsVisitor;
    }
	
	public List<PresentationElement> parse(InputStream inputstream) throws PresentationParserException {
		var presentationparserDTOs = presentationReader.read(inputstream);
		var presentationElements = new ArrayList<PresentationElement>();
		for (var presentationparserDTO : presentationparserDTOs)
		{
			presentationElements.add(presentationparserDTO.accept(mapDTOToPresentationElementsVisitor));
		}
		return presentationElements;
	}
}