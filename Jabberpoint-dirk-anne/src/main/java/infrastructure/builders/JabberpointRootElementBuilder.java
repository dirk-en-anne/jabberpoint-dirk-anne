package infrastructure.builders;

import infrastructure.parsermodels.*;

public interface JabberpointRootElementBuilder {
	public void AddSlideshow(SlideshowParserDTO slideshow);
	
	public void AddSlide(SlideParserDTO slide);
	
	public void AddComponent(SlideComponentParserDTO component);

	public JabberpointRootElement getRootElement();
}
