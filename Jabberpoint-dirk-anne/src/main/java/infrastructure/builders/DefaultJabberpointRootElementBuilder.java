package infrastructure.builders;

import infrastructure.parsermodels.*;

public class DefaultJabberpointRootElementBuilder implements JabberpointRootElementBuilder {
	private final JabberpointRootElement rootElement = new JabberpointRootElement();
	
	public void AddSlideshow(SlideshowParserDTO slideshow) {
		rootElement.getSlideshows().add(slideshow);
	}
	
	public void AddSlide(SlideParserDTO slide) {
		rootElement.getSlides().add(slide);
	}
	
	public void AddComponent(SlideComponentParserDTO component) {
		rootElement.getComponent().add(component);
	}

	public JabberpointRootElement getRootElement() {
		return rootElement;
	}
}
