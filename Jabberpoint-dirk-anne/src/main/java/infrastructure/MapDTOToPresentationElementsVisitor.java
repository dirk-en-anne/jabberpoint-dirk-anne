package infrastructure;

import domain.entities.PresentationElement;

public interface MapDTOToPresentationElementsVisitor extends MapDTOToComponentVisitor<PresentationElement> 
{
	
}
