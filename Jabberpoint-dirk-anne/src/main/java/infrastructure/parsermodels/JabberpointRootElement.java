package infrastructure.parsermodels;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "slideshows",
    "slides",
    "components"
})
@XmlRootElement(name = "Jabberpoint")
public class JabberpointRootElement {

	@XmlElement(name = "Slideshow", required = true)
    protected List<SlideshowParserDTO> slideshows;
	@XmlElement(name = "Slide", required = true)
    protected List<SlideParserDTO> slides;
	@XmlElement(name = "Component")
    protected List<SlideComponentParserDTO> components;

    public List<SlideshowParserDTO> getSlideshows() {
        if (slideshows == null) {
            slideshows = new ArrayList<SlideshowParserDTO>();
        }
        return this.slideshows;
    }

    public List<SlideParserDTO> getSlides() {
        if (slides == null) {
            slides = new ArrayList<SlideParserDTO>();
        }
        return this.slides;
    }
    
    public void setSlideshows(List<SlideshowParserDTO> slideshows) {
		this.slideshows = slideshows;
	}

	public List<SlideComponentParserDTO> getComponent() {
        if (components == null) {
            components = new ArrayList<SlideComponentParserDTO>();
        }
        return this.components;
    }
}
