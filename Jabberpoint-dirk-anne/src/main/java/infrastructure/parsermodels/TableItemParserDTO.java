package infrastructure.parsermodels;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import infrastructure.MapDTOToComponentVisitor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Table", propOrder = {
    "rows"
})
public class TableItemParserDTO extends SlideItemParserDTO
{
	@XmlElement(name = "tr", required = true)
    protected List<TableRowParserDTO> rows;

    public List<TableRowParserDTO> getRows() {
        if (rows == null) {
            rows = new ArrayList<TableRowParserDTO>();
        }
        return this.rows;
    }
    
    public void setRows(List<TableRowParserDTO> rows) {
		this.rows = rows;
	}

	@Override
	public <TComponent> TComponent accept(MapDTOToComponentVisitor<TComponent> visitor) {
		return visitor.visit(this);
	}
}
