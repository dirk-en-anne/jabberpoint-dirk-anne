package infrastructure.parsermodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LexicalItemParserDTO", propOrder = {
    "text"
})
@XmlSeeAlso({
    TitleItemParserDTO.class,
    TextItemParserDTO.class
})
public abstract class LexicalItemParserDTO extends SlideItemParserDTO
{

    @XmlElement(name = "Text", required = true)
    protected String text;

    public String getText() {
        return text;
    }

    public void setText(String value) {
        this.text = value;
    }

}
