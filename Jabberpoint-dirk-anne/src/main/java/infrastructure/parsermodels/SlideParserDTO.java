package infrastructure.parsermodels;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlType;

import infrastructure.MapDTOToComponentVisitor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SlideParserDTO", propOrder = {
    "id",
    "components"
})
public class SlideParserDTO extends PresentationParserDTO
{
	@XmlID
	@XmlElement(name = "Id", required = true)
    protected String id;
	@XmlIDREF
	@XmlElement(name = "Component")
    protected List<SlideComponentParserDTO> components;

    public String getId() {
        return id;
    }

    public void setId(String value) {
        this.id = value;
    }
    
    @Override
	public <TComponent> TComponent accept(MapDTOToComponentVisitor<TComponent> visitor) {
		return visitor.visit(this);
	}

    public List<SlideComponentParserDTO> getComponents() {
        if (components == null) {
            components = new ArrayList<SlideComponentParserDTO>();
        }
        return this.components;
    }

	public void setComponents(List<SlideComponentParserDTO> components) {
		this.components = components;
	}
}
