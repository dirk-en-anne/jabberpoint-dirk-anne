package infrastructure.parsermodels;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlType;

import infrastructure.MapDTOToComponentVisitor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SlideshowParserDTO", propOrder = {
    "title",
    "slides"
})
public class SlideshowParserDTO extends PresentationParserDTO
{	
	@XmlID
	@XmlElement(name = "Title", required = true)
    protected String title;
	
	@XmlIDREF
	@XmlElement(name = "Slide")
    protected List<SlideParserDTO> slides;
	
    public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<SlideParserDTO> getSlides() {
        if (slides == null) {
            slides = new ArrayList<SlideParserDTO>();
        }
        return this.slides;
    }
    
    public void setSlides(List<SlideParserDTO> slides) {
		this.slides = slides;
	}

	@Override
	public <TComponent> TComponent accept(MapDTOToComponentVisitor<TComponent> visitor) {
		return visitor.visit(this);
	}
}
