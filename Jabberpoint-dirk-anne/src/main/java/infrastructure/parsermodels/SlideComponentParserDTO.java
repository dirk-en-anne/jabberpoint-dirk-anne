package infrastructure.parsermodels;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlType;

import infrastructure.MapDTOToComponentVisitor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SlideComponentDTO", propOrder = {
    "id",
    "item",
    "children"
})
public class SlideComponentParserDTO extends PresentationParserDTO
{
	@XmlID
    @XmlElement(name = "Id", required = true)
    protected String id;
    @XmlElement(name = "Item")
    protected SlideItemParserDTO item;
    @XmlElement(name = "Children")
    protected SlideComponentParserDTO.Children children;

    public String getId() {
        return id;
    }

    public void setId(String value) {
        this.id = value;
    }

    public SlideItemParserDTO getItem() {
        return item;
    }

    public void setItem(SlideItemParserDTO value) {
        this.item = value;
    }

    public SlideComponentParserDTO.Children getChildren() {
        return children;
    }

    public void setChildren(SlideComponentParserDTO.Children value) {
        this.children = value;
    }
    
    @Override
	public <TComponent> TComponent accept(MapDTOToComponentVisitor<TComponent> visitor) {
		return visitor.visit(this);
	}

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "components"
    })
    public static class Children {
    	@XmlIDREF
    	@XmlElement(name = "Component")
        protected List<SlideComponentParserDTO> components;
    	
    	public List<SlideComponentParserDTO> getComponents() {
            if (components == null) {
                components = new ArrayList<SlideComponentParserDTO>();
            }
            return this.components;
        }

		public void setComponents(List<SlideComponentParserDTO> components) {
			this.components = components;
		}
    }

}
