package infrastructure.parsermodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

import infrastructure.MapDTOToComponentVisitor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Title")
public class TitleItemParserDTO extends LexicalItemParserDTO
{
	@Override
	public <TComponent> TComponent accept(MapDTOToComponentVisitor<TComponent> visitor) {
		return visitor.visit(this);
	}
}
