package infrastructure.parsermodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import infrastructure.MapDTOToComponentVisitor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PresentationParserDTO")
@XmlSeeAlso({
    SlideshowParserDTO.class,
    SlideComponentParserDTO.class,
    SlideItemParserDTO.class,
    SlideParserDTO.class
})
public abstract class PresentationParserDTO {
	public abstract <TComponent> TComponent accept(MapDTOToComponentVisitor<TComponent> visitor);
}
