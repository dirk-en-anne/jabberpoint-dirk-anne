package infrastructure.parsermodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import infrastructure.MapDTOToComponentVisitor;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Media", propOrder = {
    "source"
})
public class MediaItemParserDTO extends SlideItemParserDTO
{
    @XmlElement(name = "Source", required = true)
    protected String source;

    public String getSource() {
        return source;
    }

    public void setSource(String value) {
        this.source = value;
    }

	@Override
	public <TComponent> TComponent accept(MapDTOToComponentVisitor<TComponent> visitor) {
		return visitor.visit(this);
	}
}
