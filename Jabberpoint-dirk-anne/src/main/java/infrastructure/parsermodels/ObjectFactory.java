package infrastructure.parsermodels;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This class is used to create a XML feed using JAXB. It will automatically be resolved by JAXB, that's why you won't find any direct references.
 */
@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }
    
    public SlideParserDTO createSlideParserDTO() {
        return new SlideParserDTO();
    }

    public SlideComponentParserDTO createSlideComponentDTO() {
        return new SlideComponentParserDTO();
    }
    
    public SlideComponentParserDTO.Children createSlideComponentDTOChildren() {
        return new SlideComponentParserDTO.Children();
    }
    
    public SlideshowParserDTO createSlideshowParserDTO() {
        return new SlideshowParserDTO();
    }

    public JabberpointRootElement createJabberpoint() {
        return new JabberpointRootElement();
    }
    
    public TableItemParserDTO createTable() {
        return new TableItemParserDTO();
    }
    
    public TableRowParserDTO createTableRowParserDTO() {
        return new TableRowParserDTO();
    }
    
    public MediaItemParserDTO createMedia() {
        return new MediaItemParserDTO();
    }

    public TitleItemParserDTO createTitle() {
        return new TitleItemParserDTO();
    }

    public TextItemParserDTO createText() {
        return new TextItemParserDTO();
    }
}