package infrastructure.parsermodels;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TableRowParserDTO", propOrder = {
    "columns"
})
public class TableRowParserDTO {

	@XmlElement(name = "td", required = true)
    protected List<String> columns;

    public List<String> getColumns() {
        if (columns == null) {
            columns = new ArrayList<String>();
        }
        return this.columns;
    }

	public void setColumns(List<String> columns) {
		this.columns = columns;
	}
    
    
}
