package infrastructure.parsermodels;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SlideItemParserDTO")
@XmlSeeAlso({
    TableItemParserDTO.class,
    MediaItemParserDTO.class,
    LexicalItemParserDTO.class
})
public abstract class SlideItemParserDTO
    extends PresentationParserDTO
{


}
