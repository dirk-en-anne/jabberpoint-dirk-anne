package infrastructure;

import java.io.InputStream;
import java.util.List;

import infrastructure.parsermodels.PresentationParserDTO;

public interface PresentationReader 
{
	List<? extends PresentationParserDTO> read(InputStream inputstream) throws PresentationParserException;
}
