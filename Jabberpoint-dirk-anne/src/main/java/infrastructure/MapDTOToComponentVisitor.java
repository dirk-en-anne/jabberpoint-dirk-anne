package infrastructure;

import infrastructure.parsermodels.*;

public interface MapDTOToComponentVisitor<TComponent>
{
	TComponent visit(SlideshowParserDTO slideshow);
	TComponent visit(SlideParserDTO slide);
	TComponent visit(SlideComponentParserDTO slideComponent);
	TComponent visit(TableItemParserDTO tableItemParserDTO);
	TComponent visit(TextItemParserDTO textItemParserDTO);
	TComponent visit(TitleItemParserDTO titleItemParserDTO);
	TComponent visit(MediaItemParserDTO mediaItemParserDTO);
}
