package infrastructure;

import java.io.*;

public interface PresentationRepository 
{
	InputStream get();
	void save(String filename, byte[] data);
}
