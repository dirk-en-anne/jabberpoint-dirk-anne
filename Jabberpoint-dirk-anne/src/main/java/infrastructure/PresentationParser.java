package infrastructure;

import java.io.InputStream;
import java.util.List;

import domain.entities.PresentationElement;

public interface PresentationParser 
{
	List<PresentationElement> parse(InputStream inputstream) throws PresentationParserException;
}
