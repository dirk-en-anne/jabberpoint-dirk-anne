package infrastructure;

import java.io.OutputStream;
import java.util.List;

import javax.xml.bind.JAXBException;

import domain.entities.PresentationElement;

public interface PresentationWriter 
{
	void Write(OutputStream outputStream, List<PresentationElement> presentationElements) throws JAXBException;
}
