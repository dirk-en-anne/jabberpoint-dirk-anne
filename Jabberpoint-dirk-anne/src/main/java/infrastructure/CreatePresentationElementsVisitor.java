package infrastructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import domain.entities.PresentationElement;
import domain.factories.PresentationElementFactory;
import domain.repositories.PresentationElementCache;
import infrastructure.parsermodels.*;

public class CreatePresentationElementsVisitor implements MapDTOToPresentationElementsVisitor {

	private final PresentationElementCache cache;
	private final PresentationElementFactory factory;
	private final Map<PresentationParserDTO, Integer> levels;
	
	public CreatePresentationElementsVisitor(PresentationElementCache cache, PresentationElementFactory factory) {
		this(cache, factory, new HashMap<PresentationParserDTO, Integer>());
	}
	
	public CreatePresentationElementsVisitor(PresentationElementCache cache, PresentationElementFactory factory, Map<PresentationParserDTO, Integer> levels) {
		this.cache = cache;
		this.factory = factory;
		this.levels = levels;
	}

	@Override
	public PresentationElement visit(SlideshowParserDTO slideshowDTO) {
		var slides = new ArrayList<PresentationElement>();
		for(var slideDTO : slideshowDTO.getSlides()) {
			slides.add(slideDTO.accept(this));
		}
		return factory.createSlideshow(slideshowDTO, slides);
	}

	@Override
	public PresentationElement visit(SlideParserDTO slideDTO) 
	{
		var components = new ArrayList<PresentationElement>();
		for(var componentDTO : slideDTO.getComponents()) {
			levels.put(componentDTO, 0);
			components.add(componentDTO.accept(this));
		}
		var slide = cache.GetPresentationElement(slideDTO.getId());
		if(slide == null) {
			slide = factory.createSlide(slideDTO, components);
			cache.SetPresentationElement(slideDTO.getId(), slide);
		}
		return slide;
	}

	@Override
	public PresentationElement visit(SlideComponentParserDTO slideComponentDTO) {
		var level = levels.get(slideComponentDTO);
		var children = new ArrayList<PresentationElement>();
		children.add(slideComponentDTO.getItem() == null ? factory.createNoItem() : slideComponentDTO.getItem().accept(this));
		if(slideComponentDTO.getChildren() != null) {
			for(var childComponentDTO : slideComponentDTO.getChildren().getComponents()) {
				levels.put(childComponentDTO, level + 1);
				children.add(childComponentDTO.accept(this));
			}
		}
		
		var slideComponent = cache.GetPresentationElement(slideComponentDTO.getId());
		if(slideComponent == null) {
			slideComponent = factory.createSlideComponent(slideComponentDTO, level, children);
			cache.SetPresentationElement(slideComponentDTO.getId(), slideComponent);
		}
		return slideComponent;
	}

	@Override
	public PresentationElement visit(TableItemParserDTO tableItemParserDTO) {
		return factory.createTableItem(tableItemParserDTO);
	}

	@Override
	public PresentationElement visit(TextItemParserDTO textItemParserDTO) {
		return factory.createTextItem(textItemParserDTO);
	}

	@Override
	public PresentationElement visit(TitleItemParserDTO titleItemParserDTO) {
		return factory.createTitleItem(titleItemParserDTO);
	}

	@Override
	public PresentationElement visit(MediaItemParserDTO mediaItemParserDTO) {
		return factory.createMediaItem(mediaItemParserDTO);
	}
}
