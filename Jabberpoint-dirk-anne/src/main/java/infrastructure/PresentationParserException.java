package infrastructure;

public class PresentationParserException extends Exception {

	private static final long serialVersionUID = 4783994713350448547L;

	public PresentationParserException(String errorMessage) {
		super(errorMessage);
	}

}
