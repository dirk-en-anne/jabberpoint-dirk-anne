package infrastructure;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import infrastructure.parsermodels.JabberpointRootElement;
import infrastructure.parsermodels.PresentationParserDTO;

public class XMLPresentationReader implements PresentationReader {
	public List<? extends PresentationParserDTO> read(InputStream inputstream) throws PresentationParserException {
		try {
			var jaxbContext = JAXBContext.newInstance(JabberpointRootElement.class);
			var unmarshaller = jaxbContext.createUnmarshaller();
	
			
			var schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI); 
			var file = new File("./src/main/java/Schema/slideshow.xsd");
			var schema = schemaFactory.newSchema(file);
			unmarshaller.setSchema(schema);
	
			var rootElement = (JabberpointRootElement) unmarshaller.unmarshal(inputstream);
			return rootElement.getSlideshows();
		} 
		catch (JAXBException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		throw new PresentationParserException("Could not parse XML");
	}
}
